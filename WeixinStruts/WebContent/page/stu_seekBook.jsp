<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<title></title>
<style>
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

.panel {
	margin-top: 20px;
}
</style>
</head>
<body>
	<div class="jumbotron">
		<h6>图书详情</h6>
	</div>
	<div class="panel panel-success">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<s:property value="#book.bookname" />
		</div>
		<div class="panel-body">
			<p>图书基本信息</p>
		</div>

		<!-- List group -->
		<ul class="list-group">
			<li class="list-group-item"><strong>作者：</strong> <s:property value="#book.bookauthor" /></li>
			<li class="list-group-item"><strong>出版社：</strong><s:property value="#book.bookpress" /></li>
			<li class="list-group-item"><strong>状态：</strong><s:if test="#book.bookstatus==0">可借</s:if>
				<s:else>不可借</s:else> </li>
		</ul>
	</div>
</body>
</html>