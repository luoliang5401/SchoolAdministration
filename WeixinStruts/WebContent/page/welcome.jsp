<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%><%@ taglib prefix="s"
	uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>欢迎使用</title>
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=path%>/css/sch_style.css">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<style>
.jumbotron {
	height: 270px;
	margin-bottom: 0px;
}

.bg-primary {
	color: #fff;
	background-color: #428bca;
	margin-top: 20px;
}
</style>
</head>

<body>
	<div class="jumbotron">
		<h2>Hello, CUITER!</h2>
		<p>等你很久了...</p>
		<p>
			<button class="btn btn-primary btn-lg" role="button">know
				more</button>
		</p>
		<div class="panel" id="panel">
			<!-- Default panel contents -->
			

			<!-- List group -->
			
				<p class="bg-primary">查成绩</p>
				<p class="bg-success">查课表</p>
				<p class="bg-info">查考试</p>
				<p class="bg-warning">查自习教室</p>
				<p class="bg-danger">统统都有，等你来发掘...</p>
				
			
		</div>
	</div>
</body>
<script src="<%=path%>/js/jquery-2.0.3.min.js"></script>
<script src="<%=path%>/js/bootstrap.min.js"></script>
<script src="<%=path%>/js/bootstrap-tab.js"></script>
<script src="<%=path%>/js/application.js"></script>
<script src="<%=path%>/js/flat-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#panel").hide();
		$(".btn").click(function() {
			$("#panel").fadeToggle(1000);
		});
	});
</script>
</html>