<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.luoliang.model.*,java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%><%@ taglib prefix="s"
	uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	List<List<GradeResult>> gradelist = (List<List<GradeResult>>) request.getAttribute("gradelist");
	String name = (String)request.getAttribute("name");
%>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="<%=path%>/css/jquery-ui.min.css" rel="stylesheet"
	type="text/css" />
<link href="<%=path%>/css/style.css" rel="stylesheet" type="text/css" />
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet" type="text/css" />
<link href="<%=path%>/css/docs.css" rel="stylesheet" type="text/css" />
<style>
.jumbotron {
	height: 10px;
	background-color: #5cb85c;
	padding: 25px;
	margin-bottom: 40px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

.panel-default>.panel-heading {
	color: #333;
	background-color: #5cb85c;
	border-color: #ddd
}

p {
	text-align: center;
}

span {
	margin-left: 0px;
}

a:link {
	color: black
}

a:active {
	color: black;
}

a:visited {
	color: black
}

a:hover {
	color: black
}
</style>
<title></title>
</head>

<body>
	<div class="jumbotron">
		<h6>成绩查询</h6>
	</div>
	<div>
		<p class="lead"><%=name %>同学，你好！</p>
	</div>
	<%
		for (int i = 0; i < gradelist.size(); i++) {
			List<GradeResult> list = (List<GradeResult>) gradelist.get(i);
			if (!list.isEmpty()) {
	%>
	<div class="panel-group" id="accordion" role="tablist"
		aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion"
						href="#collapse<%=i + 1%>" aria-expanded="true"
						aria-controls="collapseOne">第<%=i + 1%>学期成绩单
					</a>
				</h4>
			</div>
			<div id="collapse<%=i + 1%>" <%if (i + 1 == 1) {%>
				class="panel-collapse collapse in" <%} else {%>
				class="panel-collapse collapse" <%}%> role="tabpanel"
				aria-labelledby="headingOne">
				<table class="table">
					<tr>
						<td>科目</td>
						<td>平时成绩</td>
						<td>总评成绩</td>
					</tr>
					<%
						for (int j = 0; j < list.size(); j++) {
									GradeResult g = list.get(j);
					%>
					<tr>
						<td class="active"><%=g.getCourse_name()%></td>
						<td class="success"><%=g.getScoreUsu()%></td>
						<td class="warning"><%=g.getScoreGer()%></td>
					</tr>
					<%
						}
					%>
				</table>
			</div>
		</div>
	</div>
	<%
		}
		}
	%>
</body>
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=path%>/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<%=path%>/js/bootstrap.min.js"></script>