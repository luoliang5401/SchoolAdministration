<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%><%@ taglib prefix="s"
	uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<title></title>
<style type="text/css">
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

.panel {
	margin-top: 20px;
}
</style>

</head>

<body>
	<div class="jumbotron">
		<h6>自习室查询</h6>
	</div>
	<s:iterator value="roomlist" id="li">
		<div class="panel panel-info">
			<div class="panel-heading">
				<s:property value="classroom_place" />
			</div>
			<div class="panel-body">
				<s:property value="classroom_status" />
			</div>
		</div>
	</s:iterator>
</body>

</html>

