<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%><%@ taglib prefix="s"
	uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>课程表查询</title>
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=path%>/css/sch_style.css">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<style>
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

li {
	margin: 0;
	font-size: 10px;
}

li>a {
	font-family: "microsoft sans serif";
	color: #000000;
	font-size: 8px;
	padding: 0px;
}

.nav-tabs>li+li {
	margin-left: 0px
}

#time {
	font-size: 40px;
	margin-top: 40px;
	margin-bottom: 20px;
}

.lead {
	margin-bottom: 0px;
	font-size: 20px;
}

tr {
	width: 60px;
}
</style>
</head>

<body>
	<div class="jumbotron">
		<h6>课程表</h6>
	</div>
	<div>
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#Mon"
				aria-controls="Mon" role="tab" data-toggle="tab">周一</a></li>
			<li role="presentation"><a href="#Tue" aria-controls="Tue"
				role="tab" data-toggle="tab">周二</a></li>
			<li role="presentation"><a href="#Wed" aria-controls="Wed"
				role="tab" data-toggle="tab">周三</a></li>
			<li role="presentation"><a href="#Thu" aria-controls="Thu"
				role="tab" data-toggle="tab">周四</a></li>
			<li role="presentation"><a href="#Fri" aria-controls="Fri"
				role="tab" data-toggle="tab">周五</a></li>
			<li role="presentation"><a href="#Sat" aria-controls="Sat"
				role="tab" data-toggle="tab">周六</a></li>
			<li role="presentation"><a href="#Sun" aria-controls="Sun"
				role="tab" data-toggle="tab">周日</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="Mon">
				<table class="table table-bordered">
					<tr>
						<td rowspan="6" width="50px" style="text-align: center;">
							<p class="lead" id="time">上午</p>
						</td>
						<td rowspan="2">1</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list0.coursename1" />
							</p> <s:if test="#list0.place1==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list0.place1" /></strong>
						</td>
					</tr>
					<tr>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">3</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list0.coursename2" />
							</p> <s:if test="#list0.place2==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list0.place2" /></strong>
					</tr>
					<tr>
						<td>4</td>
					</tr>
					<tr>
						<td rowspan="6" style="text-align: center;">
							<p class="lead" id="time">下午</p>
						<td rowspan="2">5</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list0.coursename3" />
							</p> <s:if test="#list0.place3==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list0.place3" /></strong>
					</tr>
					<tr>
						<td>6</td>
					</tr>
					<tr>
						<td rowspan="2">7</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list0.coursename4" />
							</p> <s:if test="#list0.place4==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list0.place4" /></strong>
					</tr>
					<tr>
						<td>8</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="Tue">
				<table class="table table-bordered">
					<tr>
						<td rowspan="6" width="50px" style="text-align: center;">
							<p class="lead" id="time">上午</p>
						</td>
						<td rowspan="2">1</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list1.coursename1" />
							</p> <s:if test="#list1.place1==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list1.place1" /></strong>
						</td>
					</tr>
					<tr>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">3</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list1.coursename2" />
							</p> <s:if test="#list1.place2==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list1.place2" /></strong>
					</tr>
					<tr>
						<td>4</td>
					</tr>
					<tr>
						<td rowspan="6" style="text-align: center;">
							<p class="lead" id="time">下午</p>
						<td rowspan="2">5</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list1.coursename3" />
							</p> <s:if test="#list1.place3==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list1.place3" /></strong>
					</tr>
					<tr>
						<td>6</td>
					</tr>
					<tr>
						<td rowspan="2">7</td>

					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list1.coursename4" />
							</p> <s:if test="#list1.place4==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list1.place4" /></strong>
					</tr>
					<tr>
						<td>8</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="Wed">
				<table class="table table-bordered">
					<tr>
						<td rowspan="6" width="50px" style="text-align: center;">
							<p class="lead" id="time">上午</p>
						</td>
						<td rowspan="2">1</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list2.coursename1" />
							</p> <s:if test="#list2.place1==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list2.place1" /></strong>
						</td>
					</tr>
					<tr>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">3</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list2.coursename2" />
							</p> <s:if test="#list2.place2==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list2.place2" /></strong>
					</tr>
					<tr>
						<td>4</td>
					</tr>
					<tr>
						<td rowspan="6" style="text-align: center;">
							<p class="lead" id="time">下午</p>
						<td rowspan="2">5</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list2.coursename3" />
							</p> <s:if test="#list2.place3==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list2.place3" /></strong>
					</tr>
					<tr>
						<td>6</td>
					</tr>
					<tr>
						<td rowspan="2">7</td>

					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list2.coursename4" />
							</p> <s:if test="#list2.place4==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list2.place4" /></strong>
					</tr>
					<tr>
						<td>8</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="Thu">
				<table class="table table-bordered">
					<tr>
						<td rowspan="6" width="50px" style="text-align: center;">
							<p class="lead" id="time">上午</p>
						</td>
						<td rowspan="2">1</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list3.coursename1" />
							</p> <s:if test="#list3.place1==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list3.place1" /></strong>
						</td>
					</tr>
					<tr>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">3</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list3.coursename2" />
							</p> <s:if test="#list3.place2==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list3.place2" /></strong>
					</tr>
					<tr>
						<td>4</td>
					</tr>
					<tr>
						<td rowspan="6" style="text-align: center;">
							<p class="lead" id="time">下午</p>
						<td rowspan="2">5</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list3.coursename3" />
							</p> <s:if test="#list3.place3==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list3.place3" /></strong>
					</tr>
					<tr>
						<td>6</td>
					</tr>
					<tr>
						<td rowspan="2">7</td>

					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list3.coursename4" />
							</p> <s:if test="#list3.place4==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list3.place4" /></strong>
					</tr>
					<tr>
						<td>8</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="Fri">
				<table class="table table-bordered">
					<tr>
						<td rowspan="6" width="50px" style="text-align: center;">
							<p class="lead" id="time">上午</p>
						</td>
						<td rowspan="2">1</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list4.coursename1" />
							</p> <s:if test="#list4.place1==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list4.place1" /></strong>
						</td>
					</tr>
					<tr>
						<td>2</td>
					</tr>
					<tr>
						<td rowspan="2">3</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list4.coursename2" />
							</p> <s:if test="#list4.place2==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list4.place2" /></strong>
					</tr>
					<tr>
						<td>4</td>
					</tr>
					<tr>
						<td rowspan="6" style="text-align: center;">
							<p class="lead" id="time">下午</p>
						<td rowspan="2">5</td>
					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list4.coursename3" />
							</p> <s:if test="#list4.place3==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list4.place3" /></strong>
					</tr>
					<tr>
						<td>6</td>
					</tr>
					<tr>
						<td rowspan="2">7</td>

					</tr>
					<tr>
						<td rowspan="2">
							<p class="lead">
								<s:property value="#list4.coursename4" />
							</p> <s:if test="#list4.place4==''"></s:if> <s:else>
								<span class="fui-location"></span>
							</s:else> <strong><s:property value="#list4.place4" /></strong>
					</tr>
					<tr>
						<td>8</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="Sat"></div>
			<div role="tabpanel" class="tab-pane" id="Sun"></div>

		</div>

	</div>
	<script>
		$(function() {
			$('#myTab li:eq(1) a').tab('show');
		});
	</script>
</body>
<script src="<%=path%>/js/jquery-2.0.3.min.js"></script>
<script src="<%=path%>/js/bootstrap.min.js"></script>
<script src="<%=path%>/js/bootstrap-tab.js"></script>
<script src="<%=path%>/js/application.js"></script>
<script src="<%=path%>/js/flat-ui.min.js"></script>

</html>
