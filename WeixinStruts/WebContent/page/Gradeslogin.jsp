<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<link href="<%=path%>/css/docs.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<title></title>
<style>
h4 {
	text-align: center;
	height: 70px;
	line-height: 70px;
}

p {
	text-align: center;
}

input {
	margin-top: 20px;
}

#loginbtn {
	margin-top: 20px;
}
</style>
</head>

<body>
	<div class="header">
		<h4>成都信息工程大学</h4>
		<p class="leader">成绩查询</p>
	</div>
	<div class="container">
		<form id="subUserForm">
			<div class="form">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="学号"
						name="loginname" id="id">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" placeholder="密码"
						name="password">
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-primary btn-lg btn-block"
						id="loginbtn">登录</button>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/js/jquery-2.0.3.min.js"></script>
	<script src="<%=path%>/js/sweetalert.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#loginbtn').click(function() {
				var params = $("#subUserForm").serialize();
				$.ajax({
					url : 'LoginAction_isLogin?method='+ 'queryGrade',
					type : 'post',
					data : params,
					dataType : 'json',//设置需要返回的数据类型
					success : function(data) {
						if (data.message != '') {
							sweetAlert(data.message);
						} else {
							window.location.href = "stu_queryGrades?message="+ $('#id').val();
						}
					},
				});
			});
		});
	</script>
</body>
</html>
