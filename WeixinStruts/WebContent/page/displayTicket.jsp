<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%><%@ taglib prefix="s"
	uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=path%>/css/site.min.css" rel="stylesheet">
<title></title>
<style type="text/css">
.jumbotron {
	height: 10px;
	background-color: #37BC9B;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h5 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

.panel {
	margin-top: 20px;
}

.panel-info>.panel-heading {
	color: #fff;
	background-color: #48CFAD;
	border-color: #48CFAD
}
</style>

</head>

<body>
	<div class="jumbotron">
		<h5>车票查询结果</h5>
	</div>

	<s:iterator value="ticketlist" id="li">
		<div class="panel panel-info">
			<div class="panel-heading">
				<s:property value="TrainNo" />
			</div>
			<ul class="list-group">
				<li class="list-group-item"><span class="fui-location"></span>
					<s:property value="time" /></li>
				<li class="list-group-item"><s:property value="seats" /></li>
			</ul>
		</div>
	</s:iterator>
</body>
</html>

