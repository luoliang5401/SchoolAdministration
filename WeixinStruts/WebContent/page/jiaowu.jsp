<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="<%=path %>/css/style.css" />
		<link rel="stylesheet" href="<%=path %>/css/bootstrap.min.css" />
	</head>
	<body>
		<header>
			<h2 class="head_title ">教务信息</h2>
			<h3 class="small time">发布时间：2015/10/20 17:01</h3>
		</header>
		
		<div class="content">
			<div class="main_title">
				<h3><span class="label label-success">新闻中心</span></h3>
			</div>
			<div class="main_content1">
				<ul>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1926">
						2015年首届四川科技青年创新沙龙在我校举办</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1899">
						新建学生公寓项目基础及主体顺利通过验收</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1899">
						新建学生公寓项目基础及主体顺利通过验收</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1899">
						新建学生公寓项目基础及主体顺利通过验收</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1899">
						新建学生公寓项目基础及主体顺利通过验收</a></li>
				</ul>	
			</div>
			
			<div class="main_title">
				<h3 ><span class="label label-success">重要通知</span></h3>
			</div>
			<div class="main_content2">
				<ul>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1932">
						成都信息工程大学2015年度研究生国家奖学金获奖学生建议名单公示</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1916">
						关于评定2015-2016学年国家助学金的通知</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1934">
						关于申报2016年高端外国专家（文教）引智项目计划的通知</a></li>
				</ul>
				
			</div>
			
			<div class="main_title">
				<h3><span class="label label-success">大赛风情</span></h3>
			</div>
			<div class="main_content3">
				<ul>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1923">
						第五届“通译杯”四川省口译大赛报名通知</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1895">
						关于《第三届“共享杯”大学生科技资源共享服务创新大赛》报名通知</a></li>
					<li><a href="http://www.cuit.edu.cn/ShowNews?id=1894">
						关于申报2016年高端外国专家（文教）引智项目计划的通知</a></li>
				</ul>
				
			</div>
		</div>
		<script type="text/javascript" src="<%=path %>/js/jquery-2.0.3.min.js" ></script>
		<script type="text/javascript" src="<%=path %>/js/bootstrap.min.js" ></script>
	</body>
</html>

