<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<title></title>
<style>
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

tbody td {
	font-size: 12px;
	font-family: "微软雅黑";
}
</style>
</head>

<body>
	<div class="jumbotron">
		<h6>检索结果</h6>
	</div>
	<div class="content">
		<table class="table">
			<thead>
				<tr>
					<td>书名</td>
					<td>作者</td>
				</tr>
			</thead>
			<tbody>
				<s:iterator id="li" value="booklist">
					<tr onclick="gettext(<s:property value="bookid" />)">
						<td><s:property value="bookname" /></td>
						<td><s:property value="bookauthor" /></td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
	</div>
</body>
<script src="<%=path%>/js/sweetalert.min.js"></script>
<script type="text/javascript">
function gettext(id) {
	window.location.href = "lib_seekBook?id=" + id;
}
</script>
</html>