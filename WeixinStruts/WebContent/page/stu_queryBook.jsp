<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<link href="<%=path%>/css/docs.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<title></title>
<style>
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 40px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

#search {
	margin-top: 20px;
}
</style>
</head>

<body>
	<div class="jumbotron">
		<h6>检索图书</h6>
	</div>
	<div class="container">
		<form role="form" id="subUserForm" action="lib_queryBook" method="post">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="请输入搜索内容"
					name="inputval" id="input">
			</div>
			<div class="col-md-12">
				<h6>请选择检索类型:</h6>
				<div class="form-group">
					<select data-toggle="select"
						class="form-control select select-primary select-lg"
						name="selectval" id="select">
						<option value="0" selected>书名</option>
						<option value="1">作者</option>
						<option value="2">出版社</option>
					</select>
				</div>
				<p>
					<input type="submit" class="btn btn-primary btn-lg btn-block"
						id="search" value="开始检索">
				</p>
			</div>
		</form>
	</div>
</body>
<script src="<%=path%>/js/jquery-2.1.1.min.js"></script>
<script src="<%=path%>/js/flat-ui.min.js"></script>
<script src="<%=path%>/js/application.js"></script>
<script src="<%=path%>/js/prettify.js"></script>
<script src="<%=path%>/js/sweetalert.min.js"></script>
<script type="text/javascript">
	
</script>

</html>