<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=path%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path%>/css/flat-ui.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<title></title>
<style>
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}

.panel {
	margin-top: 20px;
}
</style>
</head>
<body>
	<div class="jumbotron">
		<h6>选修列表</h6>
	</div>
	<s:iterator value="electivelist" id="elective">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<s:property value="Elective_name" />
			</div>
			<div class="panel-body">
				<p>下面的信息为选修名称，老师，地点</p>
			</div>

			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item"><span class="fui-location"></span>
				<s:property value="Elective_place" /></li>
				<li class="list-group-item"><s:property
						value="Elective_teacher" /></li>
			</ul>
		</div>
	</s:iterator>
</body>
</html>