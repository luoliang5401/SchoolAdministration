<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<%=path%>/css/jiao_style.css" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css" />
<style>
.jumbotron {
	height: 10px;
	background-color: #1abc9c;
	padding: 25px;
	margin-bottom: 0px;
}

.jumbotron h6 {
	font-family: "微软雅黑";
	text-align: center;
	margin-top: auto;
	padding: auto;
	height: 0px;
	line-height: 0px;
}
</style>
</head>
<body>
	<div class="jumbotron">
		<h6 style="font-size: 20px;">教务信息</h6>
	</div>
	<div class="content">
		<div class="main_title">
			<h3>
				<span class="label label-success">新闻中心</span>
			</h3>
		</div>
		<div class="panel panel-danger">

			<ul class="list-group">
				<s:iterator value="news">
					<li class="list-group-item"
						onclick="gettext(<s:property value="infoId" />)"><s:property
							value="infocontent" /></li>
				</s:iterator>
			</ul>

		</div>

		<div class="main_title">
			<h3>
				<span class="label label-success">重要通知</span>
			</h3>
		</div>
		<div class="panel panel-danger">

			<ul class="list-group">
				<s:iterator value="notice">
					<li class="list-group-item"
						onclick="gettext(<s:property value="infoId" />)"><s:property
							value="infocontent" /></li>
				</s:iterator>
			</ul>

		</div>
	</div>
</body>
<script type="text/javascript">
	function gettext(name) {
		window.location.href = "info_seekInfo?id=" + name;
	}
</script>
</html>