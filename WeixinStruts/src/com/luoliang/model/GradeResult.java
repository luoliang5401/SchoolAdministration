package com.luoliang.model;

public class GradeResult {
	private int id;
	private String course_name;
	private int course_time;
	private int scoreUsu;
	private int scoreGer;
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public GradeResult() {
	}

	public GradeResult(int id, String course_name, int course_time,
			int scoreUsu, int scoreGer, String username) {
		this.id = id;
		this.course_name = course_name;
		this.course_time = course_time;
		this.scoreUsu = scoreUsu;
		this.scoreGer = scoreGer;
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	public int getCourse_time() {
		return course_time;
	}

	public void setCourse_time(int course_time) {
		this.course_time = course_time;
	}

	public int getScoreUsu() {
		return scoreUsu;
	}

	public void setScoreUsu(int scoreUsu) {
		this.scoreUsu = scoreUsu;
	}

	public int getScoreGer() {
		return scoreGer;
	}

	public void setScoreGer(int scoreGer) {
		this.scoreGer = scoreGer;
	}
}
