package com.luoliang.model;

import java.util.Date;

public class Elective {
	private int Elective_id;
	private String Elective_name;
	private String Elective_teacher;
	private Date Elective_time;
	private String Elective_place;

	public int getElective_id() {
		return Elective_id;
	}

	public void setElective_id(int elective_id) {
		Elective_id = elective_id;
	}

	public String getElective_name() {
		return Elective_name;
	}

	public void setElective_name(String elective_name) {
		Elective_name = elective_name;
	}

	public String getElective_teacher() {
		return Elective_teacher;
	}

	public void setElective_teacher(String elective_teacher) {
		Elective_teacher = elective_teacher;
	}

	public Date getElective_time() {
		return Elective_time;
	}

	public void setElective_time(Date elective_time) {
		Elective_time = elective_time;
	}

	public String getElective_place() {
		return Elective_place;
	}

	public void setElective_place(String elective_place) {
		Elective_place = elective_place;
	}
}
