package com.luoliang.model;

public class ScheduleResult {
	private String coursename1;
	// private String teachname1;
	private String place1;
	private String coursename2;
	// private String teachname2;
	private String place2;
	private String coursename3;
	// private String teachname3;
	private String place3;
	private String coursename4;
	// private String teachname4;
	private String place4;

	public ScheduleResult(String coursename1, String place1,
			String coursename2, String place2, String coursename3,
			String place3, String coursename4, String place4) {
		this.coursename1 = coursename1;
		this.coursename2 = coursename2;
		this.coursename3 = coursename3;
		this.coursename4 = coursename4;
		// this.teachname1 = teachname1;
		// this.teachname2 = teachname2;
		// this.teachname3 = teachname3;
		// this.teachname4 = teachname4;
		this.place1 = place1;
		this.place2 = place2;
		this.place3 = place3;
		this.place4 = place4;
	}

	public String toString() {
		return this.coursename1 + " " + this.coursename2 + " "
				+ this.coursename3 + " " + this.coursename4 + " ";
	}

	public String getCoursename1() {
		return coursename1;
	}

	public void setCoursename1(String coursename1) {
		this.coursename1 = coursename1;
	}

	public String getPlace1() {
		return place1;
	}

	public void setPlace1(String place1) {
		this.place1 = place1;
	}

	public String getCoursename2() {
		return coursename2;
	}

	public void setCoursename2(String coursename2) {
		this.coursename2 = coursename2;
	}

	public String getPlace2() {
		return place2;
	}

	public void setPlace2(String place2) {
		this.place2 = place2;
	}

	public String getCoursename3() {
		return coursename3;
	}

	public void setCoursename3(String coursename3) {
		this.coursename3 = coursename3;
	}

	public String getPlace3() {
		return place3;
	}

	public void setPlace3(String place3) {
		this.place3 = place3;
	}

	public String getCoursename4() {
		return coursename4;
	}

	public void setCoursename4(String coursename4) {
		this.coursename4 = coursename4;
	}

	public String getPlace4() {
		return place4;
	}

	public void setPlace4(String place4) {
		this.place4 = place4;
	}
}
