package com.luoliang.model;

import java.util.Date;

public class Test {
	private int sub_id;
	private String sub_name;
	private String sub_place;
	private Date sub_time;

	public int getSub_id() {
		return sub_id;
	}

	public void setSub_id(int sub_id) {
		this.sub_id = sub_id;
	}

	public String getSub_name() {
		return sub_name;
	}

	public void setSub_name(String sub_name) {
		this.sub_name = sub_name;
	}

	public String getSub_place() {
		return sub_place;
	}

	public void setSub_place(String sub_place) {
		this.sub_place = sub_place;
	}

	public Date getSub_time() {
		return sub_time;
	}

	public void setSub_time(Date sub_time) {
		this.sub_time = sub_time;
	}

	@Override
	public String toString() {
		return "Test [sub_id=" + sub_id + ", sub_name=" + sub_name
				+ ", sub_place=" + sub_place + ", sub_time=" + sub_time + "]";
	}

}
