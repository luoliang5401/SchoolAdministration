package com.luoliang.model;

import java.util.List;

public class TranslateJson {
	private String from;
	private String to;
	private List<String> trans_result;
	private String src;
	private String dst;

	public String toString() {
		return "src:" + trans_result.get(0) + "\n" + "dst:"
				+ trans_result.get(1);
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public List<String> getTrans_result() {
		return trans_result;
	}

	public void setTrans_result(List<String> trans_result) {
		this.trans_result = trans_result;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

}
