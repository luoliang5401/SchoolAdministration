package com.luoliang.model;


public class displayTicket {
	private int id;
	private String TrainNo;
	private String time;
	private String seats;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTrainNo() {
		return TrainNo;
	}

	public void setTrainNo(String trainNo) {
		TrainNo = trainNo;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

}
