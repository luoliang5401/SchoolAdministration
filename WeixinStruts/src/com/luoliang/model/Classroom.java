package com.luoliang.model;

public class Classroom {
  
	private int classroom_id;
	private String classroom_place;
	private String classroom_status;

	public int getClassroom_id() {
		return classroom_id;
	}
	public void setClassroom_id(int classroom_id) {
		this.classroom_id = classroom_id;
	}
	public String getClassroom_place() {
		return classroom_place;
	}
	public void setClassroom_place(String classroom_place) {
		this.classroom_place = classroom_place;
	}
	public String getClassroom_status() {
		return classroom_status;
	}
	public void setClassroom_status(String classroom_status) {
		this.classroom_status = classroom_status;
	}
	
	
	
}
