package com.luoliang.model;

public class Score {
	private int score_id;
	private int student_id;
	private int scoreUsu;
	private int course_id;
	private int scoreGer;

	public int getScore_id() {
		return score_id;
	}

	public void setScore_id(int score_id) {
		this.score_id = score_id;
	}

	public int getStudent_id() {
		return student_id;
	}

	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}

	public int getScoreUsu() {
		return scoreUsu;
	}

	public void setScoreUsu(int scoreUsu) {
		this.scoreUsu = scoreUsu;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public int getScoreGer() {
		return scoreGer;
	}

	public void setScoreGer(int scoreGer) {
		this.scoreGer = scoreGer;
	}
}
