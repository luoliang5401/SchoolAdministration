package com.luoliang.model;

public class OneDayWeatherInf {
	private String location;
	String date;
	String week;
	String tempertureOfDay;
	String tempertureNow;
	String wind;
	int pmTwoPointFive;

	public OneDayWeatherInf() {

		location = "";
		date = "";
		week = "";
		tempertureOfDay = "";
		tempertureNow = "";
		wind = "";
		pmTwoPointFive = 0;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getTempertureOfDay() {
		return tempertureOfDay;
	}

	public void setTempertureOfDay(String tempertureOfDay) {
		this.tempertureOfDay = tempertureOfDay;
	}

	public String getTempertureNow() {
		return tempertureNow;
	}

	public void setTempertureNow(String tempertureNow) {
		this.tempertureNow = tempertureNow;
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public int getPmTwoPointFive() {
		return pmTwoPointFive;
	}

	public void setPmTwoPointFive(int pmTwoPointFive) {
		this.pmTwoPointFive = pmTwoPointFive;
	}

	public String toString() {
		if (tempertureNow.equals("")){
			tempertureNow = "无";
		}

		return location + "   " + week + " 全天温度：  " + tempertureOfDay
				+ " 实时天气：  " + tempertureNow + "   " + wind + "   " + "PM2.5:"
				+ pmTwoPointFive;
	}
}
