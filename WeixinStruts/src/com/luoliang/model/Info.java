package com.luoliang.model;

public class Info {
	private int infoId;
	private String infocontent;
	private String info_time;
	public int info_type;

	public int getInfo_type() {
		return info_type;
	}

	public void setInfo_type(int info_type) {
		this.info_type = info_type;
	}

	public String getInfo_time() {
		return info_time;
	}

	public void setInfo_time(String info_time) {
		this.info_time = info_time;
	}

	public int getInfoId() {
		return infoId;
	}

	public void setInfoId(int infoId) {
		this.infoId = infoId;
	}

	public String getInfocontent() {
		return infocontent;
	}

	public void setInfocontent(String infocontent) {
		this.infocontent = infocontent;
	}

}
