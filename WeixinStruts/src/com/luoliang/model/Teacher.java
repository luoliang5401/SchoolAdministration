package com.luoliang.model;

public class Teacher {
	private int t_id;
	private String t_name;
	private int t_classid;
	private String t_place;

	public int getT_classid() {
		return t_classid;
	}

	public void setT_classid(int t_classid) {
		this.t_classid = t_classid;
	}

	public String getT_place() {
		return t_place;
	}

	public void setT_place(String t_place) {
		this.t_place = t_place;
	}

	public int getT_id() {
		return t_id;
	}

	public void setT_id(int t_id) {
		this.t_id = t_id;
	}

	public String getT_name() {
		return t_name;
	}

	public void setT_name(String t_name) {
		this.t_name = t_name;
	}
}
