package com.luoliang.model;

public class stuList {
	private int id;
	private int list_class;
	private int list_grade;
	private String list_major;
	private int list_day;
	private String list_1;
	private String list_2;
	private String list_3;
	private String list_4;

	// private String list_5;
	// private String list_6;
	// private String list_7;
	// private String list_8;
	// private String list_9;
	// private String list_10;
	// private String list_11;
	// private String list_12;
	// private String list_13;
	// private String list_14;
	// private String list_15;
	// private String list_16;
	// private String list_17;
	// private String list_18;
	// private String list_19;
	// private String list_20;

	public int getId() {
		return id;
	}

	public int getList_day() {
		return list_day;
	}

	public void setList_day(int list_day) {
		this.list_day = list_day;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getList_class() {
		return list_class;
	}

	public void setList_class(int list_class) {
		this.list_class = list_class;
	}

	public int getList_grade() {
		return list_grade;
	}

	public void setList_grade(int list_grade) {
		this.list_grade = list_grade;
	}

	public String getList_major() {
		return list_major;
	}

	public void setList_major(String list_major) {
		this.list_major = list_major;
	}

	public String getList_1() {
		return list_1;
	}

	public void setList_1(String list_1) {
		this.list_1 = list_1;
	}

	public String getList_2() {
		return list_2;
	}

	public void setList_2(String list_2) {
		this.list_2 = list_2;
	}

	public String getList_3() {
		return list_3;
	}

	public void setList_3(String list_3) {
		this.list_3 = list_3;
	}

	public String getList_4() {
		return list_4;
	}

	public void setList_4(String list_4) {
		this.list_4 = list_4;
	}
}
