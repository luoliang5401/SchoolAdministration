package com.luoliang.model;

public class Course {
	private int course_id;
	private String course_name;
	private int course_teacherid;
	private int course_time;
	private int course_hours;
	private String course_Credit;

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	public int getCourse_teacherid() {
		return course_teacherid;
	}

	public void setCourse_teacherid(int course_teacherid) {
		this.course_teacherid = course_teacherid;
	}

	public int getCourse_time() {
		return course_time;
	}

	public void setCourse_time(int course_time) {
		this.course_time = course_time;
	}

	public int getCourse_hours() {
		return course_hours;
	}

	public void setCourse_hours(int course_hours) {
		this.course_hours = course_hours;
	}

	public String getCourse_Credit() {
		return course_Credit;
	}

	public void setCourse_Credit(String course_Credit) {
		this.course_Credit = course_Credit;
	}
}
