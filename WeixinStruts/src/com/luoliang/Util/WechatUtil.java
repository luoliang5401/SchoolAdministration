package com.luoliang.Util;

import java.io.IOException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.luoliang.menu.Button;
import com.luoliang.menu.Menu;
import com.luoliang.menu.ViewButton;
import com.luoliang.po.AccessToken;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class WechatUtil {
	private static final String APPID = "wx80d0e5daca21e02e";
	private static final String APPSECRET = "1540dae56a361f23a5d4df887d2cc7d6";
	private static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	// private static final String UPLOAD_URL =
	// "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
	private static final String CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";

	/**
	 * http请求方式: GET
	 * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential
	 * &appid=APPID&secret=APPSECRET
	 */
	public static JSONObject doGetStr(String url) {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		JSONObject jsonObject = null;
		try {
			HttpResponse response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				String result = EntityUtils.toString(entity, "UTF-8");
				jsonObject = JSONObject.fromObject(result);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	public static JSONObject doPostStr(String url, String outStr) {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		JSONObject jsonObject = null;
		try {
			httpPost.setEntity(new StringEntity(outStr, "UTF-8"));
			HttpResponse response = httpclient.execute(httpPost);
			String result = EntityUtils.toString(response.getEntity(), "UTF-8");
			jsonObject = JSONObject.fromObject(result);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	/**
	 * 获取access_token
	 */

	public static AccessToken getAccessToken() {
		AccessToken token = new AccessToken();
		String url = ACCESS_TOKEN_URL.replace("APPID", APPID).replace(
				"APPSECRET", APPSECRET);
		JSONObject jsonObject = doGetStr(url);
		if (jsonObject != null) {
			token.setAccess_token(jsonObject.getString("access_token"));
			token.setExpires_in(jsonObject.getInt("expires_in"));
		}

		return token;
	}

	/**
	 * 组装菜单
	 * 
	 * @return
	 */
	public static Menu initMenu() {
		Menu menu = new Menu();
		ViewButton button11 = new ViewButton();
		button11.setName("使用说明");
		button11.setType("view");
		button11.setUrl("http://luoliang5401.6655.la/WeixinStruts/page/welcome.jsp");

		ViewButton button21 = new ViewButton();
		button21.setName("移动图书馆");
		button21.setType("view");
		button21.setUrl("http://luoliang5401.6655.la/WeixinStruts/page/stu_queryBook.jsp");

		ViewButton button22 = new ViewButton();
		button22.setName("成绩查询");
		button22.setType("view");
		button22.setUrl("http://luoliang5401.6655.la/WeixinStruts/page/Gradeslogin.jsp");

		ViewButton button23 = new ViewButton();
		button23.setName("课表查询");
		button23.setType("view");
		button23.setUrl("http://luoliang5401.6655.la/WeixinStruts/page/Schedulelogin.jsp");

		ViewButton button24 = new ViewButton();
		button24.setName("选修查询");
		button24.setType("view");
		button24.setUrl("http://luoliang5401.6655.la/WeixinStruts/page/stu_queryElective.jsp");

		Button button1 = new Button();
		button1.setName("乐学成信");
		button1.setSub_button(new Button[] { button21, button22, button23,
				button24 });

		ViewButton button31 = new ViewButton();
		button31.setName("教务查询");
		button31.setType("view");
		button31.setUrl("http://luoliang5401.6655.la/WeixinStruts/info_queryEduInfo");

		ViewButton button32 = new ViewButton();
		button32.setName("考试查询");
		button32.setType("view");
		button32.setUrl("http://luoliang5401.6655.la/WeixinStruts/stu_queryTestinfo");

		ViewButton button33 = new ViewButton();
		button33.setName("自习室查询");
		button33.setType("view");
		button33.setUrl("http://luoliang5401.6655.la/WeixinStruts/stu_querySelfStudyRoom");

		Button button2 = new Button();
		button2.setName("微教务");
		button2.setSub_button(new Button[] { button31, button32, button33 });

		menu.setButton(new Button[] { button11, button1, button2 });

		return menu;
	}

	// 创建菜单
	public static int createMenu(String token, String menu)
			throws ParseException, IOException {
		int result = 0;
		String url = CREATE_MENU_URL.replace("ACCESS_TOKEN", token);
		JSONObject jsonObject = doPostStr(url, menu);
		if (jsonObject != null) {
			result = jsonObject.getInt("errcode");
		}
		return result;
	}
}
