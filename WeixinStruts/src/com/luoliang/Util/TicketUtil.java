package com.luoliang.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.luoliang.model.seatInfos;
import com.luoliang.model.trainList;

public class TicketUtil {

	public static String request(String httpUrl, String httpArg) {
		BufferedReader reader = null;
		String result = null;
		StringBuffer sbf = new StringBuffer();
		httpUrl = httpUrl + "?" + httpArg;

		try {
			URL url = new URL(httpUrl);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestMethod("GET");
			// 填入apikey到HTTP header
			connection.setRequestProperty("apikey",
					"201a6a4775ff9a4fbbbe228722e32c8c");
			connection.connect();
			InputStream is = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String strRead = null;
			while ((strRead = reader.readLine()) != null) {
				sbf.append(strRead);
				sbf.append("\r\n");
			}
			reader.close();
			result = sbf.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// 按车次详情查询
	public static List<String> searchByTrain(String from, String to,
			String date, String train) throws UnsupportedEncodingException {
		String httpUrl = "http://apis.baidu.com/qunar/qunar_train_service/traindetail";
		String httpArg = "version=1.0&train=TRAIN&from=FROM&to=TO&date=DATE"
				.replace("FROM", URLEncoder.encode(from, "utf-8"))
				.replace("TO", URLEncoder.encode(to, "utf-8"))
				.replace("TRAIN", train).replace("DATE", date);
		List<String> list = new ArrayList<String>();

		String jsonResult = request(httpUrl, httpArg);

		JSONObject dataOfJson = JSONObject.fromObject(jsonResult);
		if (dataOfJson.getString("ret") == "false") {
			list.add("车次不存在!");
		} else {
			JSONObject data = JSONObject.fromObject(dataOfJson
					.getString("data"));
			JSONObject extInfo = JSONObject.fromObject(data
					.getString("extInfo"));
			JSONObject info = JSONObject.fromObject(data.getString("info"));
			list.add("车次：" + train + "\n");
			list.add("行驶里程：" + extInfo.getString("intervalMileage") + "km\n");
			list.add("行驶时间：" + extInfo.getString("intervalTime") + "\n");
			JSONArray jsonarray = info.getJSONArray("value");
			for (int i = 0; i < jsonarray.size(); i++) {
				JSONArray a = (JSONArray) jsonarray.get(i);
				list.add("站名:" + a.get(1) + " 日期:" + a.get(2) + " 到达时间:"
						+ a.get(3) + " 开车时间:" + a.get(4) + "\n");
			}
		}

		return list;
	}

	// 站站搜索查询
	public static List<trainList> searchByStation(String from, String to,
			String date) throws UnsupportedEncodingException {
		String httpUrl = "http://apis.baidu.com/qunar/qunar_train_service/s2ssearch";
		String httpArg = "version=1.0&from=FROM&to=TO&date=DATE"
				.replace("FROM", URLEncoder.encode(from, "utf-8"))
				.replace("TO", URLEncoder.encode(to, "utf-8"))
				.replace("DATE", date);
		List<trainList> trainlist = new ArrayList<trainList>();
		String jsonResult = request(httpUrl, httpArg);
		JSONObject dataOfJson = JSONObject.fromObject(jsonResult);
		if (dataOfJson.getString("ret") == "false") {
			trainlist = null;
		} else {
			JSONObject data = JSONObject.fromObject(dataOfJson
					.getString("data"));
			if (data.getString("trainList") != "null") {
				JSONArray train = data.getJSONArray("trainList");
				for (int i = 0; i < train.size(); i++) {
					JSONObject json = train.getJSONObject(i);
					trainList t = new trainList();
					t.setTrainType("车型:" + json.getString("trainType"));
					t.setTrainNo("车次:" + json.getString("trainNo"));
					t.setStartTime("出发时间:" + json.getString("startTime"));
					t.setEndTime("到达时间:" + json.getString("endTime"));
					t.setDuration("运行时间:" + json.getString("duration"));
					JSONArray seat = json.getJSONArray("seatInfos");
					List<seatInfos> info = new ArrayList<seatInfos>();
					for (int j = 0; j < seat.size(); j++) {
						JSONObject arr = JSONObject.fromObject(seat.get(j));
						seatInfos s = new seatInfos();
						s.setSeat("座位类型：" + arr.getString("seat"));
						s.setSeatPrice("价格：" + arr.getString("seatPrice"));
						s.setRemainNum("余票数：" + arr.getString("remainNum"));
						info.add(s);
					}
					t.setSeatlist(info);

					trainlist.add(t);
				}
			} else {
				trainlist = null;

			}
		}

		return trainlist;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		//System.out.println(MessageUtil.initticketText("成都", "西昌", "2015-12-18"));
		System.out.println(MessageUtil.initticketText1("#车次#成都,重庆,20151225,g308"));
	}

}
