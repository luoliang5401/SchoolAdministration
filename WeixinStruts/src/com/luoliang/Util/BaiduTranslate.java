package com.luoliang.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.luoliang.model.TranslateJson;

public class BaiduTranslate {
	public static String getTranslate(String q, String lang) {
		StringBuffer strBuf;
		String baiduUrl = null;
		String salt = Long.toString(new Date().getTime());
		try {
			baiduUrl = "http://api.fanyi.baidu.com/api/trans/vip/translate?q="
					+ URLEncoder.encode(q, "utf-8") + "&from=auto&to=" + lang
					+ "&appid=20151211000007676&salt=" + salt + "&sign="
					+ CheckUtil.md5(q, salt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strBuf = new StringBuffer();

		try {
			URL url = new URL(baiduUrl);
			URLConnection conn = url.openConnection();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream(), "utf-8"));// ת�롣
			String line = null;
			while ((line = reader.readLine()) != null)
				strBuf.append(line + " ");
			reader.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return strBuf.toString();
	}

	public static TranslateJson resolveTranslateInf(String strPar) {
		JSONObject dataOfJson = JSONObject.fromObject(strPar);
		TranslateJson json = new TranslateJson();

		String from = dataOfJson.getString("from");
		String to = dataOfJson.getString("to");
		json.setFrom(from);
		json.setTo(to);

		JSONArray results = dataOfJson.getJSONArray("trans_result");
		JSONObject results0 = results.getJSONObject(0);

		String src = results0.getString("src");
		String dst = results0.getString("dst");
		List<String> list = new ArrayList<String>();
		list.add(src);
		list.add(dst);
		json.setTrans_result(list);

		return json;
	}

	public static void main(String[] args) {
		System.out.println(resolveTranslateInf(getTranslate("moon", "zh"))); 
	}
}