package com.luoliang.Util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.luoliang.model.OneDayWeatherInf;
import com.luoliang.model.TranslateJson;
import com.luoliang.model.WeatherInf;
import com.luoliang.model.displayTicket;
import com.luoliang.model.trainList;
import com.luoliang.po.Image;
import com.luoliang.po.ImageMessage;
import com.luoliang.po.News;
import com.luoliang.po.NewsMessage;
import com.luoliang.po.TextMessage;
import com.sun.org.apache.xml.internal.serializer.ToStream;
import com.thoughtworks.xstream.XStream;

/**
 * @author Chambers
 *
 */
public class MessageUtil {
	public static final String MESSAGE_TEXT = "text";
	public static final String MESSAGE_NEWS = "news";
	public static final String MESSAGE_IMAGE = "image";
	public static final String MESSAGE_VOICE = "voice";
	public static final String MESSAGE_VIDEO = "video";
	public static final String MESSAGE_LINK = "link";
	public static final String MESSAGE_LOCATION = "location";
	public static final String MESSAGE_EVENT = "event";
	public static final String MESSAGE_SUBSCRIBE = "subscribe";
	public static final String MESSAGE_UNSUBSCRIBE = "unsubscribe";
	public static final String MESSAGE_CLICK = "CLICK";
	public static final String MESSAGE_VIEW = "VIEW";
	public static final String MESSAGE_SCANCODE = "scancode_push";

	public static Map<String, String> xmlToMap(HttpServletRequest request)
			throws IOException, DocumentException {
		Map<String, String> map = new HashMap<String, String>();
		SAXReader reader = new SAXReader();

		InputStream ins = request.getInputStream();
		Document doc = reader.read(ins);

		Element root = doc.getRootElement();

		@SuppressWarnings("unchecked")
		List<Element> list = root.elements();

		for (Element e : list) {
			map.put(e.getName(), e.getText());
		}

		ins.close();
		return map;

	}

	// 文本对象转换为xml方法
	public static String textMessageToXml(TextMessage textMessage) {
		XStream xstream = new XStream();
		xstream.alias("xml", textMessage.getClass());
		return xstream.toXML(textMessage);
	}

	public static String initText(String ToUserName, String FromUserName,
			String Content) {
		TextMessage text = new TextMessage();
		text.setFromUserName(ToUserName);
		text.setToUserName(FromUserName);
		text.setMsgType(MessageUtil.MESSAGE_TEXT);
		text.setCreateTime(new Date().getTime());
		text.setContent(Content);
		return MessageUtil.textMessageToXml(text);
	}

	// 主菜单
	public static String menuText() {
		StringBuffer sb = new StringBuffer();
		sb.append("欢迎您的关注~等你好久了~~\n\n");
		sb.append("本微信平台由Moon团队自主开发，请放心使用。\n\n");
		sb.append("所有功能请在菜单栏进行点击体验。\n\n");
		sb.append("发送#天气#+地点可查询天气。\n\n");
		sb.append("发送#中#/#英#+想翻译的内容可分别翻译成中文和英文。\n\n");
		sb.append("发送#车次#+起点站+终点站+日期+车次可查询车次，比如#车次#上海，北京，20160101，G102\n\n");
		sb.append("发送#车站#+起点站+终点站+日期可查询所有列车，比如#车站#上海，北京，20160101\n\n");
		sb.append("更多功能尽请期待...");

		return sb.toString();
	}

	// 生成翻译的内容
	public static String initTranslateText(String str) {
		String transInf = str.substring(3);
		StringBuffer sb = new StringBuffer();
		TranslateJson json = new TranslateJson();

		if (str.contains("#中#")) {
			json = BaiduTranslate.resolveTranslateInf(BaiduTranslate
					.getTranslate(transInf, "zh"));
		}
		if (str.contains("#英#")) {
			json = BaiduTranslate.resolveTranslateInf(BaiduTranslate
					.getTranslate(transInf, "en"));
		}

		sb.append("原文:" + json.getTrans_result().get(0) + "\n\n");
		sb.append("译文:" + json.getTrans_result().get(1));

		return sb.toString();
	}

	// 生成天气信息
	public static String initweatherText(String str) {
		String city = str.substring(4);
		WeatherInf info = new WeatherInf();
		StringBuffer sb = new StringBuffer();

		info = WeatherUtil
				.resolveWeatherInf(WeatherUtil.getWeatherInform(city));
		OneDayWeatherInf[] oneday = new OneDayWeatherInf[4];
		oneday = info.getWeatherInfs();

		sb.append(info.getColdAdvise() + "\n\n");
		sb.append(oneday[0] + "\n\n");
		sb.append(oneday[1] + "\n\n");
		sb.append(oneday[2] + "\n\n");

		return sb.toString();
	}

	public static String initDate(String str) {
		StringBuilder sb = new StringBuilder();
		sb.append(str.substring(0, 4) + "-");
		sb.append(str.substring(4, 6) + "-");
		sb.append(str.substring(6));

		return sb.toString();
	}

	// 生成按车站查询车票信息
	public static List<displayTicket> initticketText(String from, String to,
			String date) throws UnsupportedEncodingException {
		List<trainList> trainlist = TicketUtil.searchByStation(from, to, date);
		List<displayTicket> list1 = new ArrayList<displayTicket>();
		if (trainlist != null) {
			for (int i = 0; i < trainlist.size(); i++) {
				StringBuffer sb = new StringBuffer();
				trainList t = trainlist.get(i);
				displayTicket dt = new displayTicket();
				dt.setTrainNo(t.getTrainNo()+" "+t.getTrainType());
				dt.setTime(t.getStartTime()+" " + t.getEndTime() + " "
						+ t.getDuration());
				for (int j = 0; j < t.getSeatlist().size(); j++) {
					sb.append(t.getSeatlist().get(j)).append("\n");
				}
				dt.setSeats(sb.toString());
				list1.add(dt);
			}
		}

		return list1;
	}

	public static String initticketNewsMessage(String ToUserName,
			String FromUserName, String Content)
			throws UnsupportedEncodingException {
		if (Content.contains("，") == true) {
			Content = Content.replaceAll("，", ",");
		}
		String message = null;
		List<News> newsList = new ArrayList<News>();
		NewsMessage newsMessage = new NewsMessage();
		Content = Content.substring(4);
		String[] arg = new String[3];
		arg = Content.split(",");
		String date = initDate(arg[2]);
		String from = URLEncoder.encode(arg[0], "UTF-8");
		String to = URLEncoder.encode(arg[1], "UTF-8");

		News news = new News();
		news.setTitle("查询结果");
		news.setDescription("点击查看您输入的车次信息");
		news.setPicUrl("http://luoliang5401.6655.la/WeixinStruts/Pic/chepiao.jpg");
		news.setUrl("http://luoliang5401.6655.la/WeixinStruts/displayTicket?from="
				+ from + "&to=" + to + "&date=" + date); // 点击的跳转地址

		newsList.add(news);

		newsMessage.setToUserName(FromUserName);
		newsMessage.setFromUserName(ToUserName);
		newsMessage.setCreateTime(new Date().getTime());
		newsMessage.setMsgType(MESSAGE_NEWS);
		newsMessage.setArticles(newsList);
		newsMessage.setArticleCount(newsList.size());

		message = newsMessageToXml(newsMessage);

		return message;
	}

	// 生成按车次查询车票信息
	public static String initticketText1(String str)
			throws UnsupportedEncodingException {
		str = str.substring(4);
		if (str.contains("，") == true) {
			str = str.replaceAll("，", ",");
			System.out.println(str);
		}
		StringBuffer sb = new StringBuffer();
		String[] arg = new String[4];
		arg = str.split(",");
		List<String> info = new ArrayList<String>();
		String date = initDate(arg[2]);
		if (info.isEmpty()) {
			info = TicketUtil.searchByTrain(arg[0], arg[1], date, arg[3]);
			for (int i = 0; i < info.size(); i++) {
				sb.append(info.get(i));
			}
		} else
			sb.append("查询信息错误");

		return sb.toString();
	}

	/**
	 * 图文消息转换为xml
	 * 
	 * 
	 * */
	public static String newsMessageToXml(NewsMessage newsMessage) {
		XStream xstream = new XStream();
		xstream.alias("xml", newsMessage.getClass());
		xstream.alias("item", new News().getClass());
		return xstream.toXML(newsMessage);
	}

	/**
	 * 图片消息转为xml
	 * 
	 * @param imageMessage
	 * @return
	 */
	public static String imageMessageToXml(ImageMessage imageMessage) {
		XStream xstream = new XStream();
		xstream.alias("xml", imageMessage.getClass());
		return xstream.toXML(imageMessage);
	}

	public static String initNewsMessage(String ToUserName, String FromUserName) {
		String message = null;
		List<News> newsList = new ArrayList<News>();
		NewsMessage newsMessage = new NewsMessage();

		News news = new News();
		news.setTitle("成信大介绍");
		news.setDescription("成都信息工程大学（Chengdu University of Information Technology）坐落于有“天府之国”之称的历史文化名城成都，由四川省人民政府和中国气象局共建，入选中国首批“卓越工程师教育培养计划”、“中西部高校基础能力建设工程”、“2011计划”，为国际CDIO组织正式成员、中国气象人才培养联盟成员，是第一所为中国人民解放军第二炮兵部队培养国防生的一般本科院校，是以信息学科和大气学科为重点，学科交叉为特色，工学、理学、管理学为主要学科门类的省属重点大学。");
		news.setPicUrl("http://luoliang5401.6655.la/WeixinStruts/Pic/schooldoor.jpg");
		news.setUrl("www.cuit.edu.cn"); // 点击的跳转地址

		newsList.add(news);

		newsMessage.setToUserName(FromUserName);
		newsMessage.setFromUserName(ToUserName);
		newsMessage.setCreateTime(new Date().getTime());
		newsMessage.setMsgType(MESSAGE_NEWS);
		newsMessage.setArticles(newsList);
		newsMessage.setArticleCount(newsList.size());

		message = newsMessageToXml(newsMessage);

		return message;
	}

	/**
	 * 组装图片消息
	 * 
	 * @param toUserName
	 * @param fromUserName
	 * @return
	 */
	public static String initImageMessage(String toUserName, String fromUserName) {
		String message = null;
		Image image = new Image();
		image.setMediaId("WpwL9jOPKMHZr1QaIxIZG2_k68Ujy6hkS1lmSBpD86KAq22njEHRu2mPcHqgWN1h");
		ImageMessage imageMessage = new ImageMessage();
		imageMessage.setFromUserName(toUserName);
		imageMessage.setToUserName(fromUserName);
		imageMessage.setMsgType(MESSAGE_IMAGE);
		imageMessage.setCreateTime(new Date().getTime());
		imageMessage.setImage(image);
		message = imageMessageToXml(imageMessage);
		return message;
	}
}
