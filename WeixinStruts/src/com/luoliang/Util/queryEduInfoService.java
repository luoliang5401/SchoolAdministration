package com.luoliang.Util;

import java.util.ArrayList;
import java.util.List;

import com.luoliang.model.Info;

public class queryEduInfoService {
	List<Info> list;

	public List queryInfo() {
		list = HibernateUtils.queryList("from Info");
		List<Info> news = new ArrayList<Info>();
		List<Info> notice = new ArrayList<Info>();

		for (int i = 0; i < list.size(); i++) {
			Info info = list.get(i);
			if (info.getInfo_type() == 0) {
				news.add(info);
			}
			if (info.getInfo_type() == 1) {
				notice.add(info);
			}
		}

		List<List<Info>> all = new ArrayList<List<Info>>();
		all.add(news);
		all.add(notice);

		return all;
	}
}
