package com.luoliang.Util;

import java.util.ArrayList;
import java.util.List;

import com.luoliang.model.Elective;

public class queryElectiveService {
	List<Elective> list;

	@SuppressWarnings("unchecked")
	public List<Elective> findElective(String inputval, String selectval) {
		if (selectval.equals("0")) {
			list = (List<Elective>) HibernateUtils.queryElective(inputval, selectval);
		}

		if (selectval.equals("1")) {
			list = (List<Elective>) HibernateUtils.queryElective(inputval, selectval);
		}

		if (selectval.equals("2")) {
			list = (List<Elective>) HibernateUtils.queryElective(inputval, selectval);
		}

		if (list == null) {

			list = new ArrayList<Elective>();
			Elective b = new Elective();
			b.setElective_name("没有此课程，请换个姿势重新搜索重新搜索");
			list.add(b);

		}

		return list;
	}
}
