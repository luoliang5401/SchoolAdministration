package com.luoliang.Util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Date;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class CheckUtil {
	private static final String token = "weixin";

	public static boolean checkSignature(String signature, String timestamp,
			String nonce) {
		String arr[] = new String[] { token, timestamp, nonce };

		// 排序
		Arrays.sort(arr);

		// 生成字符串
		StringBuffer content = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			content.append(arr[i]);

		}

		// sha1加密
		String temp = getSha1(content.toString());

		return temp.equals(signature);
	}

	public static String getSha1(String str) {
		if (null == str || 0 == str.length()) {
			return null;
		}
		char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
			mdTemp.update(str.getBytes("UTF-8"));

			byte[] md = mdTemp.digest();
			int j = md.length;
			char[] buf = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
				buf[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(buf);
		} catch (Exception e) {
			return null;
		}
	}

	public static String md5(String query,String SALT) throws IOException {

		String APPID = "20151211000007676";
		String KEY = "SfsTfRDb6ej78BWBbYq_";
		String SIGN = "";

		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("javascript");

		//File directory = new File("");// 设定为当前文件夹
		String jsFileName = "D:/workspace/WeixinStruts/src/com/luoliang/Util/md5.js";
			//	+ "/src/com/luoliang/Util/md5.js"; // 读取js文件/WeixinStruts/src/com/luoliang/Util/md5.js
		FileReader reader;
		try {
			reader = new FileReader(jsFileName);
			engine.eval(reader);
			if (engine instanceof Invocable) {
				Invocable invoke = (Invocable) engine; // 调用merge方法，并传入两个参数
				SIGN = (String) invoke.invokeFunction("MD5", APPID + query
						+ SALT + KEY);
				System.out.println(jsFileName);
			}

			reader.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 执行指定脚本
		return SIGN;

	}
}
