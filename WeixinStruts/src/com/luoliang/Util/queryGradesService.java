package com.luoliang.Util;

import java.util.ArrayList;
import java.util.List;

import com.luoliang.model.GradeResult;

public class queryGradesService {
	Object[] object;
	private List<List<GradeResult>> resultlist = new ArrayList<List<GradeResult>>();
	private List<GradeResult> list1 = new ArrayList<GradeResult>();
	private List<GradeResult> list2 = new ArrayList<GradeResult>();
	private List<GradeResult> list3 = new ArrayList<GradeResult>();

	public List<List<GradeResult>> stuGrades(String value) {
		String sql = "select u.id,c.course_name,c.course_time, s.scoreUsu,s.scoreGer,u.username "
				+ "from User u, Course c, Score s "
				+ "where u.loginname=s.student_id and c.course_id=s.course_id and u.loginname="
				+ value;
		List list = HibernateUtils.queryList(sql);
		GradeResult g;
		for (int i = 0; i < list.size(); i++) {   //依次判断每学期的课程成绩
			object = (Object[]) list.get(i);
			if ((int) object[2] == 1) {
				g = new GradeResult((int) object[0], (String) object[1],
						(int) object[2], (int) object[3], (int) object[4],
						(String) object[5]);
				list1.add(g);
			} else if ((int) object[2] == 2) {
				g = new GradeResult((int) object[0], (String) object[1],
						(int) object[2], (int) object[3], (int) object[4],
						(String) object[5]);
				list2.add(g);
			} else if ((int) object[2] == 3) {
				g = new GradeResult((int) object[0], (String) object[1],
						(int) object[2], (int) object[3], (int) object[4],
						(String) object[5]);
				list3.add(g);
			}
		}
		resultlist.add(list1);
		resultlist.add(list2);
		resultlist.add(list3);

		return resultlist;
	}
}
