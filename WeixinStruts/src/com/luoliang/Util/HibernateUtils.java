package com.luoliang.Util;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	private static SessionFactory sessionfactory;

	private HibernateUtils() {
	}

	static {
		Configuration cfg = new Configuration().configure();
		sessionfactory = cfg.buildSessionFactory();
	}

	public static Session getSession() {
		return sessionfactory.openSession();
	}

	// 添加
	public static void add(Object obj) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtils.getSession();
			tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		System.out.println("add成功");
	}

	// 修改
	public static void update(Object obj) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtils.getSession();
			tx = session.beginTransaction();
			session.update(obj);
			tx.commit();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		System.out.println("update成功");
	}

	// 删除
	public static void delete(Object obj) {
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateUtils.getSession();
			tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		System.out.println("delete成功");
	}

	// 按照id查找
	public static Object findById(Class clazz, Serializable id) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Object obj = session.get(clazz, id);
			System.out.println("find成功");
			return obj;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	// 根据其他属性查找
	public static Object HQLselectVal(String str) {
		Session session = null;
		session = HibernateUtils.getSession();
		Query query = session
				.createQuery("from User u where u.loginname = :login");
		query.setParameter("login", str);

		List list = query.list();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public static List queryList(String sql) {
		Session session = null;
		Transaction tx = null;
		List list = null;
		try {
			session = HibernateUtils.getSession();
			tx = session.beginTransaction();
			Query query = session.createQuery(sql);
			list = query.list();
			tx.commit();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		System.out.println("查询list成功");

		return list;
	}

	// 按照不同的筛选条件搜索图书
	public static Object querybook(String str, String val) {
		Session session = null;
		session = HibernateUtils.getSession();
		Query query = null;
		if (val.equals("0")) {
			query = session
					.createQuery("from Book b where b.bookname like :input");
		}
		if (val.equals("1")) {
			query = session
					.createQuery("from Book b where b.bookauthor like :input");
		}
		if (val.equals("2")) {
			query = session
					.createQuery("from Book b where b.bookpress like :input");
		}
		query.setParameter("input", "%" + str + "%");

		List list = query.list();
		if (list.size() > 0) {
			return list;
		} else {
			return null;
		}
	}

	//根据不同的条件查询选修
	public static Object queryElective(String str, String val) {
		Session session = null;
		session = HibernateUtils.getSession();
		Query query = null;
		if (val.equals("0")) {
			query = session
					.createQuery("from Elective b where b.Elective_name like :input");
		}
		if (val.equals("1")) {
			query = session
					.createQuery("from Elective b where b.Elective_teacher like :input");
		}
		if (val.equals("2")) {
			query = session
					.createQuery("from Elective b where b.Elective_place like :input");
		}
		query.setParameter("input", "%" + str + "%");

		List list = query.list();
		
		if (list.size() > 0) {
			return list;
		} else {
			return null;
		}
	}
}
