package com.luoliang.test;

import java.io.IOException;
import net.sf.json.JSONObject;
import com.luoliang.Util.WechatUtil;
import com.luoliang.po.AccessToken;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class WechatTest {
	public static void main(String[] args) throws ParseException, IOException {
		AccessToken token = WechatUtil.getAccessToken();
		System.out.println("票据:" + token.getAccess_token());
		System.out.println("有效时间:" + token.getExpires_in());

		String menu = JSONObject.fromObject(WechatUtil.initMenu()).toString();
		int result = WechatUtil.createMenu(token.getAccess_token(), menu);
		if (result == 0) {
			System.out.println("创建菜单成功！");
		} else {
			System.out.println("错误:" + result);
		}
	}
}
