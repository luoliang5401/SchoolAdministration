package com.luoliang.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.MessageUtil;
import com.luoliang.model.displayTicket;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class displayTicketAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String execute() throws UnsupportedEncodingException {
		HttpServletRequest req = ServletActionContext.getRequest();
		String from = URLDecoder.decode(req.getParameter("from"), "UTF-8");
		String to = URLDecoder.decode(req.getParameter("to"), "UTF-8");
		String date = req.getParameter("date");
		List<displayTicket> list = MessageUtil.initticketText(from, to, date);

		ActionContext ctx = ActionContext.getContext();
		ctx.put("ticketlist", list);

		return SUCCESS;
	}
}
