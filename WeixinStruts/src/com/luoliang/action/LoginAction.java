package com.luoliang.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.LoginService;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String loginname;
	String password;
	String message;
	HttpServletRequest req = ServletActionContext.getRequest();

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String isLogin() {
		System.out.println(req.getParameter("method"));
		String method = req.getParameter("method");
		LoginService ls = new LoginService();

		if (ls.isLogin(loginname, password)) {
			message = "";
			if (method.equals("queryGrade")) { // 如果为查成绩返回查成绩结果
				return SUCCESS;
			} else if (method.equals("querySchedule")) {
				return SUCCESS;
			} else if (method.equals("queryTestinfo")) {
				return SUCCESS;
			} else {
				return SUCCESS;
			}
		} else {
			message = "用户名或密码错误！";

			return INPUT;
		}
	}

	public String reLogin() {
		return "reloginGrades";
	}

	public String reloginSchedule() {
		return "reloginSchedule";
	}
}
