package com.luoliang.action;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.dom4j.DocumentException;

import com.luoliang.Util.CheckUtil;
import com.luoliang.Util.MessageUtil;
import com.opensymphony.xwork2.ActionSupport;

public class WeChatAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String execute() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String signature = request.getParameter("signature");
		String timestamp = request.getParameter("timestamp");
		String nonce = request.getParameter("nonce");
		String echostr = request.getParameter("echostr");

		PrintWriter out = response.getWriter();
		if (CheckUtil.checkSignature(signature, timestamp, nonce)) {
			out.print(echostr);
		}

		PrintWriter out1 = response.getWriter();
		try {
			Map<String, String> map = MessageUtil.xmlToMap(request);
			String FromUserName = map.get("FromUserName");
			String ToUserName = map.get("ToUserName");
			String MsgType = map.get("MsgType");
			String Content = map.get("Content");
			String message = null;

			if (MessageUtil.MESSAGE_TEXT.equals(MsgType)) {
				if ("1".equals(Content)) {
					message = MessageUtil.initText(ToUserName, FromUserName,
							"谢谢使用，菜单1待开发");
				} else if ("2".equals(Content)) {
					message = MessageUtil.initNewsMessage(ToUserName,
							FromUserName);
				} else if (Content.contains("#天气#")) {
					message = MessageUtil.initText(ToUserName, FromUserName,
							MessageUtil.initweatherText(Content));
				} else if (Content.contains("#中#") || Content.contains("#英#")) {
					message = MessageUtil.initText(ToUserName, FromUserName,
							MessageUtil.initTranslateText(Content));
				} else if (Content.contains("#车次#")) {
					message = MessageUtil.initText(ToUserName, FromUserName,
							MessageUtil.initticketText1(Content));
				} else if (Content.contains("#车站#")) {
					message = MessageUtil.initticketNewsMessage(ToUserName,
							FromUserName, Content);
				} else {
					message = MessageUtil.initText(ToUserName, FromUserName,
							MessageUtil.menuText());
				}
			} else if (MessageUtil.MESSAGE_EVENT.equals(MsgType)) {
				String eventType = map.get("Event");
				if (MessageUtil.MESSAGE_SUBSCRIBE.equals(eventType)) {
					message = MessageUtil.initText(ToUserName, FromUserName,
							MessageUtil.menuText());
				} else if (MessageUtil.MESSAGE_CLICK.equals(eventType)) {
					message = MessageUtil.initText(ToUserName, FromUserName,
							MessageUtil.menuText());
				} else if (MessageUtil.MESSAGE_VIEW.equals(eventType)) {
					String url = map.get("EventKey");
					message = MessageUtil.initText(ToUserName, FromUserName,
							url);
				} else if (MessageUtil.MESSAGE_SCANCODE.equals(eventType)) {
					String key = map.get("EventKey");
					message = MessageUtil.initText(ToUserName, FromUserName,
							key);
				}
			} else if (MessageUtil.MESSAGE_LOCATION.equals(MsgType)) {
				String label = map.get("Label");
				message = MessageUtil.initText(ToUserName, FromUserName, label);
			}
			System.out.println(message);
			out1.print(message);
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}

		return SUCCESS;
	}
}
