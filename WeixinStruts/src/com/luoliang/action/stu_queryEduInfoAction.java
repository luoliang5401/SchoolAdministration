package com.luoliang.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.HibernateUtils;
import com.luoliang.Util.queryEduInfoService;
import com.luoliang.model.Info;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_queryEduInfoAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String queryEduInfo() {
		queryEduInfoService qe = new queryEduInfoService();
		ActionContext ctx = ActionContext.getContext();

		List list = qe.queryInfo();

		ctx.put("news", list.get(0));
		ctx.put("notice", list.get(1));

		return "queryEduInfo";
	}

	public String seekInfo() {
		HttpServletRequest req = ServletActionContext.getRequest();
		Info info = (Info) HibernateUtils.findById(Info.class,
				Integer.parseInt(req.getParameter("id")));

		ActionContext ctx = ActionContext.getContext();
		
		ctx.put("info", info);

		return "seekInfo";
	}
}
