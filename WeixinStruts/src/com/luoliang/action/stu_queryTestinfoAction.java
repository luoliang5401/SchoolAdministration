package com.luoliang.action;

import java.util.List;

import com.luoliang.Util.queryTestinfoService;
import com.luoliang.model.Test;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_queryTestinfoAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Test> list;
	queryTestinfoService qs = new queryTestinfoService();

	public String queryTestinfo() {
		ActionContext ctx = ActionContext.getContext();

		list = qs.findselfstudyroom();

		ctx.put("testlist", list);

		return "queryTest";
	}

}