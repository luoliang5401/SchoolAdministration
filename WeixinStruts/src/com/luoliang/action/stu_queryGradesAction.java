package com.luoliang.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.HibernateUtils;
import com.luoliang.Util.queryGradesService;
import com.luoliang.model.GradeResult;
import com.luoliang.model.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_queryGradesAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	User stu = new User();

	public User getStu() {
		return stu;
	}

	public void setStu(User stu) {
		this.stu = stu;
	}

	public String queryGrades() {
		ActionContext ct = ActionContext.getContext();
		HttpServletRequest req = ServletActionContext.getRequest();
		List<List<GradeResult>> resultlist = new ArrayList<List<GradeResult>>();
		queryGradesService qgs = new queryGradesService();

		resultlist = qgs.stuGrades(req.getParameter("message"));
		@SuppressWarnings("unchecked")
		List<User> user = HibernateUtils
				.queryList("from User u where u.loginname="
						+ req.getParameter("message"));

		ct.put("gradelist", resultlist);
		ct.put("name", user.get(0).getUsername());

		return "queryGrades";
	}
}
