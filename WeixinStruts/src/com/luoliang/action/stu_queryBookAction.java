package com.luoliang.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.HibernateUtils;
import com.luoliang.Util.queryBookService;
import com.luoliang.model.Book;
import com.luoliang.model.Info;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_queryBookAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String inputval;
	private String selectval;
	HttpServletRequest req = ServletActionContext.getRequest();
	List<Book> list;

	public String getInputval() {
		return inputval;
	}

	public void setInputval(String inputval) {
		this.inputval = inputval;
	}

	public String getSelectval() {
		return selectval;
	}

	public void setSelectval(String selectval) {
		this.selectval = selectval;
	}

	public String queryBook() {
		ActionContext ct = ActionContext.getContext();
		queryBookService qb = new queryBookService();

		list = qb.findBooks(inputval, selectval);

		ct.put("booklist", list);

		return "queryBooks";
	}

	public String seekBook() {
		HttpServletRequest req = ServletActionContext.getRequest();
		
		Book book = (Book) HibernateUtils.findById(Book.class,
				Integer.parseInt(req.getParameter("id")));

		ActionContext ctx = ActionContext.getContext();

		ctx.put("book", book);

		return "seekInfo";
	}
}
