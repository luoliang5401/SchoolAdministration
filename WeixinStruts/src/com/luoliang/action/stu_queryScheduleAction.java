package com.luoliang.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.queryScheduleService;
import com.luoliang.model.ScheduleResult;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_queryScheduleAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String querySchedule() {
		ActionContext ct = ActionContext.getContext();
		HttpServletRequest req = ServletActionContext.getRequest();
		queryScheduleService util = new queryScheduleService();

		List list = util.ScheduleMothod(req.getParameter("message"));

		for (int i = 0; i < list.size(); i++) {
			ct.put("list" + i, (ScheduleResult) list.get(i));
		}

		return "querySchedule";
	}
}
