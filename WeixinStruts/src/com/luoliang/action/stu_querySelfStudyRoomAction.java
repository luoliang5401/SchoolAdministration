package com.luoliang.action;

import java.util.List;

import com.luoliang.Util.querySelfStudyRoomService;
import com.luoliang.model.Classroom;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_querySelfStudyRoomAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Classroom> list;
	querySelfStudyRoomService qs = new querySelfStudyRoomService();

	public String querySelfStudyRoom() {
		ActionContext ctx = ActionContext.getContext();

		list = qs.findselfstudyroom();

		ctx.put("roomlist", list);

		return "queryRoom";
	}

}
