package com.luoliang.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.luoliang.Util.queryElectiveService;
import com.luoliang.model.Elective;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class stu_queryElectiveAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String inputval;
	private String selectval;
	HttpServletRequest req = ServletActionContext.getRequest();
	List<Elective> list;

	public String getInputval() {
		return inputval;
	}

	public void setInputval(String inputval) {
		this.inputval = inputval;
	}

	public String getSelectval() {
		return selectval;
	}

	public void setSelectval(String selectval) {
		this.selectval = selectval;
	}

	public String queryElective() {
		ActionContext ct = ActionContext.getContext();
		queryElectiveService qb = new queryElectiveService();

		list = qb.findElective(inputval, selectval);

		ct.put("electivelist", list);

		return "queryElective";
	}

}
