package com.lib.action;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.Utils.StringToDate;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.Test;

public class TestAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Test test;
	private List<Test> list;
	private String id;
	private Test findtest;
	HibernateUtilsInterface userDao;
	HttpServletRequest req = ServletActionContext.getRequest();

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Test> getList() {
		return list;
	}

	public void setList(List<Test> list) {
		this.list = list;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public TestAction() {
	}

	/**
	 * 添加考试安排
	 * 
	 * @return
	 */
	public String add() {

		return "add";
	}

	public String list() {

		list = userDao.queryList("from Test");
		// System.out.print(list.get(1).toString());
		ActionContext ct = ActionContext.getContext();
		ct.put("testlist", list);
		return "list";
	}

	public String save() {

		try {

			userDao.add(test);
			req.setCharacterEncoding("UTF-8");
			req.setAttribute("tipMessage", "添加成功！");
			System.out.println("添加考试成功...");
		} catch (Exception e) {

		}

		return "add";
	}

	public String queryByid() {

		findtest = (Test) userDao.findbyId(Integer.parseInt(id), "Test");
		req.setAttribute("findtest", findtest);

		return "findtest";
	}

	public String update() throws ParseException {
		StringToDate std = new StringToDate();
		test.setSub_time(std.todate(std.DatetoStr(test.getSub_time())));
		userDao.update(test);
		req.setAttribute("tipMessage", "修改成功！");
		System.out.println("修改考试成功...");

		return "updatetest";
	}

	public String delete() {
		int id = Integer.parseInt(req.getParameter("id"));
		test = (Test) userDao.findbyId(id, "Test");
		userDao.delete(test);
		req.setAttribute("tipMessage", "删除成功！");
		System.out.println("删除考试成功...");

		return "deletetest";
	}

}
