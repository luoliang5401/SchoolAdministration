package com.lib.action;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.Book;

public class libAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Book book = new Book();
	List<Book> list;
	HibernateUtilsInterface userDao;
	HttpServletRequest req = ServletActionContext.getRequest();
	private String id;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<Book> getList() {
		return list;
	}

	public void setList(List<Book> list) {
		this.list = list;
	}

	@SuppressWarnings("unchecked")
	public String querylist() {
		list = (List<Book>) userDao.queryList("from Book");
		ActionContext ct = ActionContext.getContext();
		ct.put("list", list);

		return "querylist";
	}

	public String save() {

		try {
			if (status.equals("可借")) {
				book.setBookstatus(0);
			} else {
				book.setBookstatus(1);
			}
			userDao.add(book);
			req.setCharacterEncoding("UTF-8");
			req.setAttribute("tipMessage", "添加成功！");
			System.out.println("添加图书成功...");
		} catch (Exception e) {

		}

		return "add";
	}

	public String queryByid() {
		ActionContext ctx = ActionContext.getContext();
		
		book = (Book) userDao.findbyId(Integer.parseInt(id), "Book");
		ctx.put("book", book);

		return "findbook";
	}

	public String update() throws ParseException {
		userDao.update(book);
		req.setAttribute("tipMessage", "修改成功！");
		System.out.println("修改图书成功...");

		return "update";
	}

	public String delete() {
		int id = Integer.parseInt(req.getParameter("id"));
		book = (Book) userDao.findbyId(id, "Book");
		userDao.delete(book);
		req.setAttribute("tipMessage", "删除成功！");
		System.out.println("删除图书成功...");

		return "delete";
	}

	public String add() {

		return "add";
	}
}
