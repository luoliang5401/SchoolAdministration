package com.lib.action;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.Classroom;

public class ClassRoomAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Classroom classroom;
	private List<Classroom> list;
	HibernateUtilsInterface userDao;
	private String id;
	HttpServletRequest req = ServletActionContext.getRequest();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public ClassRoomAction() {
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public List<Classroom> getList() {
		return list;
	}

	public void setList(List<Classroom> list) {
		this.list = list;
	}

	@SuppressWarnings("unchecked")
	public String list() {
		list = userDao.queryList("from Classroom");

		ActionContext ct = ActionContext.getContext();
		ct.put("classlist", list);
		return "list";
	}

	public String add() {

		return "add";
	}

	public String save() {
		try {
			userDao.add(classroom);
			HttpServletRequest request = ServletActionContext.getRequest();
			request.setCharacterEncoding("UTF-8");
			request.setAttribute("tipMessage", "添加成功！");

		} catch (Exception e) {
			// TODO: handle exception
		}

		return "add";
	}

	public String queryByid() {
		classroom = (Classroom) userDao.findbyId(Integer.parseInt(id),
				"Classroom");
		req.setAttribute("user1", classroom);

		return "findclass";
	}

	public String delete() {
		classroom = (Classroom) userDao.findbyId(Integer.parseInt(id),
				"Classroom");
		userDao.delete(classroom);
		req.setAttribute("tipMessage", "删除成功！");
		System.out.println("删除自习室成功...");

		return "delete";
	}

	public String update() throws ParseException {
		userDao.update(classroom);
		req.setAttribute("tipMessage", "修改成功！");
		System.out.println("修改自习室成功...");

		return "update";
	}

}
