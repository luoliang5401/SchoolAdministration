package com.lib.action;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.Utils.StringToDate;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.Elective;

public class ElectiveAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Elective elective;
	private List<Elective> list;
	private String id;
	HibernateUtilsInterface userDao;
	HttpServletRequest req = ServletActionContext.getRequest();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public Elective getElective() {
		return elective;
	}

	public void setElective(Elective elective) {
		this.elective = elective;
	}

	public List<Elective> getList() {
		return list;
	}

	public void setList(List<Elective> list) {
		this.list = list;
	}

	public String list() {
		list = userDao.queryList("from Elective");

		ActionContext ct = ActionContext.getContext();
		ct.put("Eleclist", list);
		return "list";
	}

	public String add() {

		return "add";
	}

	public String save() {
		try {
			userDao.add(elective);
			req.setCharacterEncoding("UTF-8");
			req.setAttribute("tipMessage", "添加成功！");
		} catch (Exception e) {
			// TODO: handle exception
		}

		return "add";
	}

	public String update() throws ParseException {
		StringToDate std = new StringToDate();
		elective.setElective_time(std.todate(std.DatetoStr(elective
				.getElective_time())));
		userDao.update(elective);
		req.setAttribute("tipMessage", "修改成功！");
		System.out.println("修改选修成功...");

		return "updateelective";
	}

	public String queryByid() {
		elective = (Elective) userDao
				.findbyId(Integer.parseInt(id), "Elective");
		req.setAttribute("user1", elective);

		return "findelective";
	}

	public String deleteEletive() {
		elective = (Elective) userDao
				.findbyId(Integer.parseInt(id), "Elective");
		userDao.delete(elective);
		req.setAttribute("tipMessage", "删除成功！");
		System.out.println("删除考试成功...");

		return "delete";
	}
}
