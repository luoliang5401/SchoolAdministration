package com.lib.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.Info;
import com.school.po.Test;

public class InfAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Info info;
	private List<Info> list;
	HibernateUtilsInterface userDao;
	HttpServletRequest req = ServletActionContext.getRequest();

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public List<Info> getList() {
		return list;
	}

	public void setList(List<Info> list) {
		this.list = list;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public InfAction() {

	}

	public String list() {
		list = userDao.queryList("from Info");

		ActionContext ct = ActionContext.getContext();
		ct.put("inflist", list);

		return "list";
	}

	public String add() {

		return "add";
	}

	public String save() {
		try {
			userDao.add(info);
			HttpServletRequest request = ServletActionContext.getRequest();
			request.setCharacterEncoding("UTF-8");
			request.setAttribute("tipMessage", "添加成功！");

		} catch (Exception e) {
			// TODO: handle exception
		}

		return "add";
	}

	public String deleteByid() {
		int id = Integer.parseInt(req.getParameter("id"));
		info = (Info) userDao.findbyId(id, "Info");
		userDao.delete(info);
		req.setAttribute("tipMessage", "删除成功！");
		System.out.println("删除通知成功...");

		return "deleteInfo";

	}

	public String edit() {
		int id = Integer.parseInt(req.getParameter("id"));
		info = (Info) userDao.findbyId(id, "Info");
		
		ActionContext ctx = ActionContext.getContext();
		ctx.put("Info", info);
		
		return "edit";
	}
	
	public String update() {
		userDao.update(info);
		
		req.setAttribute("tipMessage", "更新成功！");
		System.out.println("更新通知成功...");
		
		return "editInfo";
	}
}
