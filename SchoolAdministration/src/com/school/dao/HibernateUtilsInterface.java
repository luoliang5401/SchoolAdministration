package com.school.dao;

import java.util.List;

public interface HibernateUtilsInterface {
	public void add(Object obj);

	public void update(Object obj);

	public void delete(Object obj);

	public List queryList(String sql);

	public Object findbyId(int id, String classz);
}
