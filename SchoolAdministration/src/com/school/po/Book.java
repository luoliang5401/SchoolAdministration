package com.school.po;

public class Book {
	private int bookid;
	private String bookname;
	private String bookauthor;
	private String bookpress;
	private int readid;
	private int bookstatus;


	public int getBookid() {
		return bookid;
	}

	public void setBookid(int bookid) {
		this.bookid = bookid;
	}

	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	public String getBookauthor() {
		return bookauthor;
	}

	public void setBookauthor(String bookauthor) {
		this.bookauthor = bookauthor;
	}

	public String getBookpress() {
		return bookpress;
	}

	public void setBookpress(String bookpress) {
		this.bookpress = bookpress;
	}

	public int getReadid() {
		return readid;
	}

	public void setReadid(int readid) {
		this.readid = readid;
	}

	public int getBookstatus() {
		return bookstatus;
	}

	public void setBookstatus(int bookstatus) {
		this.bookstatus = bookstatus;
	}


}
