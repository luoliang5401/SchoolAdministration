package com.school.Utils;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.school.dao.HibernateUtilsInterface;

public class HibernateUtils extends HibernateDaoSupport implements
		HibernateUtilsInterface {

	public void add(Object obj) {
		getHibernateTemplate().save(obj);
	}

	public void update(Object obj) {
		getHibernateTemplate().update(obj);
	}

	public void delete(Object obj) {
		getHibernateTemplate().delete(obj);
	}

	public List queryList(String sql) {
		List list = getHibernateTemplate().find(sql);

		return list;
	}

	@Override
	public Object findbyId(int id, String classz) {
		List list = getHibernateTemplate().find(
				"from " + classz + " where id = " + id);

		if (list.size() > 0) {
			Object t = list.get(0);

			return t;
		}

		return null;
	}

	// private static SessionFactory sessionfactory;
	//
	// private HibernateUtils() {
	// }
	//
	// static {
	// Configuration cfg = new Configuration().configure();
	// sessionfactory = cfg.buildSessionFactory();
	// }
	//
	// public static Session getSession() {
	// return sessionfactory.openSession();
	// }
	//
	// // 添加
	// public static void add(Object obj) {
	// Session session = null;
	// Transaction tx = null;
	// try {
	// session = HibernateUtils.getSession();
	// tx = session.beginTransaction();
	// session.save(obj);
	// tx.commit();
	// } finally {
	// if (session != null) {
	// session.close();
	// }
	// }
	// System.out.println("add成功");
	// }
	//
	// // 修改
	// public static void update(Object obj) {
	// Session session = null;
	// Transaction tx = null;
	// try {
	// session = HibernateUtils.getSession();
	// tx = session.beginTransaction();
	// session.update(obj);
	// tx.commit();
	// } finally {
	// if (session != null) {
	// session.close();
	// }
	// }
	// System.out.println("update成功");
	// }
	//
	// // 删除
	// public static void delete(Object obj) {
	// Session session = null;
	// Transaction tx = null;
	// try {
	// session = HibernateUtils.getSession();
	// tx = session.beginTransaction();
	// session.delete(obj);
	// tx.commit();
	// } finally {
	// if (session != null) {
	// session.close();
	// }
	// }
	// System.out.println("delete成功");
	// }
	//
	// // 查找
	// public static Object findById(Class clazz, Serializable id) {
	// Session session = null;
	// try {
	// session = HibernateUtils.getSession();
	// Object obj = session.get(clazz, id);
	// System.out.println("find成功");
	// return obj;
	// } finally {
	// if (session != null) {
	// session.close();
	// }
	// }
	// }
	//
	// public static List queryList(String sql) {
	// Session session = null;
	// Transaction tx = null;
	// List list = null;
	// try {
	// session = HibernateUtils.getSession();
	// tx = session.beginTransaction();
	// Query query = session.createQuery(sql);
	// list = query.list();
	// tx.commit();
	// } finally {
	// if (session != null) {
	// session.close();
	// }
	// }
	// System.out.println("查询list成功");
	//
	// return list;
	// }
}
