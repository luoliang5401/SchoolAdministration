package com.student.action;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.school.Utils.StringToDate;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.User;

public class student_editAction extends ActionSupport implements
		ModelDriven<User> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	User stu = new User();
	HttpServletRequest req = ServletActionContext.getRequest();
	HibernateUtilsInterface userDao;

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public String edit() throws UnsupportedEncodingException, ParseException {
		StringToDate std = new StringToDate();
		stu.setBirthday(std.todate(std.DatetoStr(stu.getBirthday())));
		userDao.update(stu);
		req.setAttribute("user1", stu);
		req.setAttribute("tipMessage", "�޸ĳɹ���");
		System.out.println(stu.getUsername());
		System.out.println("this is in student_editAction");

		return "editStu";
	}

	public User getModel() {
		return stu;
	}
}
