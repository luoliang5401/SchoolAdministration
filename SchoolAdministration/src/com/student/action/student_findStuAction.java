package com.student.action;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.Utils.StringToDate;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.User;

public class student_findStuAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HibernateUtilsInterface userDao;

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	User user = new User();
	private String time;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String findStu() throws UnsupportedEncodingException, ParseException {
		List<User> list = userDao.queryList("from User");
		ActionContext ct = ActionContext.getContext();
		// 查询到的列表放入值栈中
		ct.put("list", list);
		user = (User) userDao.findbyId(Integer.parseInt(ServletActionContext
				.getRequest().getParameter("id")), "User");
		HttpServletRequest req = ServletActionContext.getRequest();
		StringToDate std = new StringToDate();
		req.setAttribute("user1", user);
		req.setAttribute("time", std.DatetoStr(user.getBirthday()));
		ct.put("user",user);
		ct.put("time",std.DatetoStr(user.getBirthday()));
		if (user != null)
			System.out.println("找到的学号为：" + user.getLoginname());

		return "findStu";
	}
}
