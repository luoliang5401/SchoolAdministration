package com.student.action;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	String adm;
	String pwd;

	public String getAdm() {
		return adm;
	}

	public void setAdm(String adm) {
		this.adm = adm;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String islogin() {

		// HttpServletRequest request = ServletActionContext.getRequest();
		if (!adm.equals("admin") || !pwd.equals("admin")) {
			message = "用户名或密码错误!";
			// request.setAttribute("message", message);
			return INPUT;
		} else if (adm.equals("") || pwd.equals("")) {
			message = "用户名和密码不能为空!";
			// request.setAttribute("message", message);
			return INPUT;
		} else {
			message = "";

			return "toUse";
		}
	}

	public String login() {
		return "login";
	}

	public String use() {

		return "use";
	}

}
