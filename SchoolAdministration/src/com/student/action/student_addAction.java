package com.student.action;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.User;

public class student_addAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	User user = new User();
	HibernateUtilsInterface userDao;

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String add() throws UnsupportedEncodingException, ParseException {
		user.setGrade(Integer.parseInt(user.getLoginname().substring(0, 4)));
		userDao.add(user);
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setCharacterEncoding("UTF-8");
		request.setAttribute("tipMessage", "���ӳɹ���");
		System.out.println("this is in student_addAction");

		return "add";
	}

}
