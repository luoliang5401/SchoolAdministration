package com.student.action;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.User;

public class student_querylistAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<User> list;
	HibernateUtilsInterface userDao;

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

	@SuppressWarnings("unchecked")
	public String querylist() {
		list = (List<User>)userDao.queryList("from User");
		ActionContext ct = ActionContext.getContext();
		ct.put("list", list);

		return "querylist";
	}
}
