package com.student.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.school.dao.HibernateUtilsInterface;
import com.school.po.User;

public class student_deleteAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	User stu = new User();
	HttpServletRequest req = ServletActionContext.getRequest();
	HibernateUtilsInterface userDao;

	public void setUserDao(HibernateUtilsInterface userDao) {
		this.userDao = userDao;
	}

	public String delete() {
		int id = Integer.parseInt(req.getParameter("id"));
		stu = (User) userDao.findbyId(id, "User");
		userDao.delete(stu);
		System.out.println("这是在删除id为" + stu.getId() + "的学生中....");

		return "deleteStu";
	}

}
