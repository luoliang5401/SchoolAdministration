<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*,com.school.po.*,com.school.Utils.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	Test u = (Test) request.getAttribute("findtest");
%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>CUIT微信校务管理平台</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap.css" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<%=path%>/css/uniform.css" />
<link rel="stylesheet" href="<%=path%>/css/select2.css" />
<link rel="stylesheet" href="<%=path%>/css/unicorn.main.css" />
<link rel="stylesheet" href="<%=path%>/css/unicorn.grey.css"
	class="skin-color" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<div id="header">
		<h2>CUIT微信</h2>
		<h3>校务管理平台</h3>
	</div>

	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>查看学生</a>
		<ul>
			<li><a href="<%=path%>/Page/index.jsp"><i
					class="icon icon-home"></i> <span>首页</span></a></li>
			<li class="active submenu open"><a href="#"><i
					class="icon icon-th-list"></i> <span>教务通知</span> <span
					class="label">2</span></a>
				<ul>
					<li><a href="test_list">考试安排</a></li>
					<li><a href="inf_list">信息与公告</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-th"></i>
					<span>选修和教室查询</span> <span class="label">2</span></a>
				<ul>
					<li><a href="elective_list">公共选修</a></li>
					<li><a href="classroom_list">自习室安排</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i
					class="icon icon-pencil"></i> <span>图书管理</span><span class="label">2</span></a>
				<ul>
					<li><a href="lib_add">添加图书</a></li>
					<li><a href="lib_querylist">图书列表</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-bell"></i>
					<span>学籍信息</span> <span class="label">2</span></a>
				<ul>
					<li><a href="<%=path%>/Page/student_add.jsp">添加学生</a></li>
					<li class="active"><a href="student_querylistAction">查看学生</a></li>
				</ul></li>
		</ul>

	</div>

	<div id="content">
		<div id="content-header">
			<h1>考试信息</h1>
		</div>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i
				class="icon-home"></i> 主页</a> <a href="#" class="current">编辑考试信息</a>
		</div>

		<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog bs-example-modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="exampleModalLabel">考试信息</h4>
					</div>
					<div class="modal-body">
						<form class="form-inline" action="test_update" method="post">
							<input type="hidden" value="<%=u.getSub_id()%>"
								name="test.sub_id" id="id">
							<div class="form-group">
								<label for="exampleInputName2" style="margin: 20px">考试科目</label>
								<input class="form-control" type="text" name="test.sub_name"
									value="<%=u.getSub_name()%>">
							</div>
							<div class="form-group">
								<label for="exampleInputName2" style="margin: 20px">考试地点</label>
								<input class="form-control" type="text" name="test.sub_place"
									value="<%=u.getSub_place()%>" />
							</div>
							<div class="form-group">
								<label for="exampleInputName2" style="margin: 20px">考试时间</label>
								<input class="form-control" type="date" name="test.sub_time"
									value="<%=u.getSub_time()%>" />
							</div>

							<div class="modal-footer">
								<a type="button" class="btn btn-default" href="test_list">返回</a>
								<button type="submit" class="btn btn-primary" id="editbtn">修改</button>
								<a type="button" class="btn btn-danger" onclick="del()">删除</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="<%=path%>/js/jquery.min.js"></script>
	<script src="<%=path%>/js/jquery.ui.custom.js"></script>
	<script src="<%=path%>/js/bootstrap.min.js"></script>
	<script src="<%=path%>/js/jquery.uniform.js"></script>
	<script src="<%=path%>/js/select2.min.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"></script>
	<script src="<%=path%>/js/unicorn.js"></script>
	<script src="<%=path%>/js/unicorn.tables.js"></script>
	<script src="<%=path%>/js/sweetalert.min.js"></script>
	<script type="text/javascript">
		function del() {
			swal("删除成功！", "You clicked the button!", "success");
			setTimeout(function() {
				window.location.href = "test_delete?id="
						+ document.getElementById('id').value;
			}, 2000);
		}
		$(function() {
			var msg = "${request.tipMessage}";
			if (msg != "") {
				swal(msg, "You clicked the button!", "success");
				setTimeout(function() {
					window.location.href = "test_upd?id="
							+ document.getElementById('id').value;
				}, 2000);
			}
		});
		$(function() {
			$('#myModal').modal("show");
		});
		$(function() {
			$('#myModal').on('hide.bs.modal', function() {
				window.location.href = "student_querylistAction";
			})
		});
	</script>
</body>

</html>