<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<html lang="en">
<!-- container-fluid -->

<head>
<title>CUIT微信校务管理平台</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=path%>/css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="<%=path%>/css/fullcalendar.css">
<link rel="stylesheet" href="<%=path%>/css/unicorn.main.css">
<link rel="stylesheet" href="<%=path%>/css/unicorn.grey.css"
	class="skin-color">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>

	<div id="header">
		<h2>CUIT微信</h2>
		<h3>校务管理平台</h3>
	</div>

	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>
			CUIT校务管理系统</a>
		<ul>
			<li class="active"><a href="<%=path%>/Page/index.jsp"><i
					class="icon icon-home"></i> <span>首页</span></a></li>
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>教务通知</span> <span class="label">2</span></a>
				<ul>
					<li><a href="test_list">考试安排</a></li>
					<li><a href="inf_list">信息与公告</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-th"></i>
					<span>选修和教室查询</span> <span class="label">2</span></a>
				<ul>
					<li><a href="elective_list">公共选修</a></li>
					<li><a href="classroom_list">自习室安排</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-pencil"></i>
					<span>图书管理</span><span class="label">2</span></a>
				<ul>
					<li><a href="lib_add">添加图书</a></li>
					<li><a href="lib_querylist">图书列表</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-bell"></i>
					<span>学籍信息</span> <span class="label">2</span></a>
				<ul>
					<li><a href="<%=path%>/Page/student_add.jsp">添加学生</a></li>
					<li><a href="student_querylistAction">查看学生</a></li>
				</ul></li>
		</ul>

	</div>
	<div id="style-switcher">
		<i class="icon-arrow-left icon-white"></i> <span>Style:</span> <a
			href="#grey"
			style="background-color: #555555; border-color: #aaaaaa;"></a> <a
			href="#blue" style="background-color: #2D2F57;"></a> <a href="#red"
			style="background-color: #673232;"></a>
	</div>

	<div id="content">
		<div id="content-header">
			<h1>欢迎使用</h1>
		</div>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i
				class="icon-home"></i> 主页</a> <a href="#" class="current">欢迎使用</a>
		</div>
		<div class="jumbotron">
			<h1>Hello, CUITER!</h1>
			<h4>欢迎使用CUIT校务管理系统</h4>
		</div>
	</div>

	<script src="<%=path%>/js/excanvas.min.js"></script>
	<script src="<%=path%>/js/jquery.min.js"></script>
	<script src="<%=path%>/js/jquery.ui.custom.js"></script>
	<script src="<%=path%>/js/bootstrap.min.js"></script>
	<script src="<%=path%>/js/jquery.flot.min.js"></script>
	<script src="<%=path%>/js/jquery.flot.resize.min.js"></script>
	<script src="<%=path%>/js/jquery.peity.min.js"></script>
	<script src="<%=path%>/js/fullcalendar.min.js"></script>
	<script src="<%=path%>/js/unicorn.js"></script>
	<script src="<%=path%>/js/unicorn.dashboard.js"></script>
</body>

</html>