<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<html lang="en">

<head>
<title>CUIT微信校务管理平台</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=path%>/css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="<%=path%>/css/colorpicker.css">
<link rel="stylesheet" href="<%=path%>/css/datepicker.css">
<link rel="stylesheet" href="<%=path%>/css/uniform.css">
<link rel="stylesheet" href="<%=path%>/css/select2.css">
<link rel="stylesheet" href="<%=path%>/css/unicorn.main.css">
<link rel="stylesheet" href="<%=path%>/css/unicorn.grey.css"
	class="skin-color">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
.controls input {
	height: 30px;
}
</style>
</head>

<body>

	<div id="header">
		<h2>CUIT微信</h2>
		<h3>校务管理平台</h3>
	</div>

	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>添加学生</a>
		<ul>
			<li><a href="<%=path%>/Page/index.jsp"><i
					class="icon icon-home"></i> <span>首页</span></a></li>
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>教务通知</span> <span class="label">2</span></a>
				<ul>
					<li><a href="test_list">考试安排</a></li>
					<li><a href="inf_list">信息与公告</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-th"></i>
					<span>选修和教室查询</span> <span class="label">2</span></a>
				<ul>
					<li><a href="elective_list">公共选修</a></li>
					<li><a href="classroom_list">自习室安排</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-pencil"></i>
					<span>图书管理</span><span class="label">2</span></a>
				<ul>
					<li><a href="lib_add">添加图书</a></li>
					<li><a href="lib_querylist">图书列表</a></li>
				</ul></li>
			<li class="active submenu open"><a href="#"><i
					class="icon icon-bell"></i> <span>学籍信息</span> <span class="label">2</span></a>
				<ul>
					<li class="active"><a href="<%=path%>/Page/student_add.jsp">添加学生</a></li>
					<li><a href="student_querylistAction">查看学生</a></li>
				</ul></li>
		</ul>

	</div>
	<div id="style-switcher">
		<i class="icon-arrow-left icon-white"></i> <span>Style:</span> <a
			href="#grey"
			style="background-color: #555555; border-color: #aaaaaa;"></a> <a
			href="#blue" style="background-color: #2D2F57;"></a> <a href="#red"
			style="background-color: #673232;"></a>
	</div>

	<div id="content">
		<div id="content-header">
			<h1>添加学生</h1>
		</div>
		<div id="breadcrumb">
			<a href="" title="Go to Home" class="tip-bottom"><i
				class="icon-home"></i> 主页</a> <a href="#" class="current">添加学生</a>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-justify"></i>
							</span>
							<h5>信息录入</h5>
						</div>
						<div class="widget-content nopadding">
							<form action="student_addAction" method="post"
								class="form-horizontal">
								<div class="control-group">
									<div class="controls">
										<input type="hidden" name="user.id">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">学号</label>
									<div class="controls">
										<input type="text" name="user.loginname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">密码</label>
									<div class="controls">
										<input type="password" name="user.password">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">姓名</label>
									<div class="controls">
										<input type="text" name="user.username">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">年龄</label>
									<div class="controls">
										<input type="text" name="user.age" maxlength="3">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">生日</label>
									<div class="controls">
										<input type="date" name="user.birthday">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">性别</label>
									<div class="controls">
										<select class="form-control" style="width: 200px;"
											name="user.sex">
											<option selected>男</option>
											<option>女</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">专业</label>
									<div class="controls">
										<select class="form-control" style="width: 200px;"
											name="user.major">
											<option selected>软件工程</option>
											<option>计算机科学与技术</option>
											<option>大气科学</option>
											<option>网络工程</option>
											<option>通信工程</option>
											<option>电子信息工程</option>
											<option>自动化</option>
											<option>英语</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">班级</label>
									<div class="controls">
										<select class="form-control" style="width: 200px;"
											name="user.clas">
											<option selected>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
											<option>6</option>
										</select>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary"
										style="margin-left: 120px;" id="add-button">确认添加</button>
									<button type="reset" class="btn btn-danger"
										style="margin-left: 90px;">取消添加</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<%=path%>/js/jquery.min.js"></script>
	<script src="<%=path%>/js/jquery.ui.custom.js"></script>
	<script src="<%=path%>/js/bootstrap.min.js"></script>
	<script src="<%=path%>/js/bootstrap-colorpicker.js"></script>
	<script src="<%=path%>/js/bootstrap-datepicker.js"></script>
	<script src="<%=path%>/js/jquery.uniform.js"></script>
	<script src="<%=path%>/js/sweetalert.min.js"></script>
	<script src="<%=path%>/js/unicorn.js"></script>
	<script src="<%=path%>/js/unicorn.form_common.js"></script>
	<script type="text/javascript">
		var msg = "${request.tipMessage}";
		if (msg != "") {
			swal(msg, "You clicked the button!", "success")
			request.removeAttribute("tipMessage");
		}
	</script>
</body>

</html>