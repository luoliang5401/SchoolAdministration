<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<html lang="en">

<head>
<title>CUIT微信校务管理平台</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<%=path%>/css/uniform.css" />
<link rel="stylesheet" href="<%=path%>/css/select2.css" />
<link rel="stylesheet" href="<%=path%>/css/unicorn.main.css" />
<link rel="stylesheet" href="<%=path%>/css/buttons.css" />
<link rel="stylesheet" href="<%=path%>/css/unicorn.grey.css"
	class="skin-color" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<div id="header">
		<h2>CUIT微信</h2>
		<h3>校务管理平台</h3>
	</div>

	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>自习室安排</a>
		<ul>
			<li><a href="<%=path%>/Page/index.jsp"><i
					class="icon icon-home"></i> <span>首页</span></a></li>
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>教务通知</span> <span class="label">2</span></a>
				<ul>
					<li><a href="test_list">考试安排</a></li>
					<li><a href="inf_list">信息与公告</a></li>
				</ul></li>
			<li class="active submenu open"><a href="#"><i
					class="icon icon-th"></i> <span>选修和教室查询</span> <span class="label">2</span></a>
				<ul>
					<li><a href="elective_list">公共选修</a></li>
					<li><a href="classroom_list">自习室安排</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i
					class="icon icon-pencil"></i> <span>图书管理</span><span class="label">2</span></a>
				<ul>
					<li><a href="lib_add">添加图书</a></li>
					<li><a href="lib_querylist">图书列表</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-bell"></i>
					<span>学籍信息</span> <span class="label">2</span></a>
				<ul>
					<li><a href="<%=path%>/Page/student_add.jsp">添加学生</a></li>
					<li><a href="student_querylistAction">查看学生</a></li>
				</ul></li>
		</ul>


	</div>


	<div id="content">
		<div id="content-header">
			<h1>查看自习室</h1>
		</div>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i
				class="icon-home"></i> Home</a> <a href="#" class="current">自习教室列表</a>
		</div>
		<a href="classroom_add" class="btn btn-primary"
			style="margin-left: 20px; margin-top: 20px">添加自习教室</a>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<div class="widget-box">
						<div class="widget-title">
							<h5>学生列表</h5>
						</div>
						<div class="widget-content nopadding">
							<table class="table table-bordered data-table">
								<thead>
									<tr>
										<th>自习室位置</th>
										<th>自习室状态</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<s:iterator id="li" value="classlist">
										<tr>
											<th><s:property value="classroom_place" /></th>
											<th><s:property value="classroom_status" /></th>
											<th><a type="button" class="btn btn-primary"
												style="height: 20px; font-size: 6px"
												href="classroom_queryByid?id=<s:property value="classroom_id" />">查看</a></th>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<%=path%>/js/jquery.min.js"></script>
	<script src="<%=path%>/js/jquery.ui.custom.js"></script>
	<script src="<%=path%>/js/bootstrap.min.js"></script>
	<script src="<%=path%>/js/jquery.uniform.js"></script>
	<script src="<%=path%>/js/select2.min.js"></script>
	<script src="<%=path%>/js/jquery.dataTables.min.js"></script>
	<script src="<%=path%>/js/unicorn.js"></script>
	<script src="<%=path%>/js/unicorn.tables.js"></script>
</body>

</html>