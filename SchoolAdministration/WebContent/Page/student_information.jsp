<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.*,com.school.po.*,com.school.Utils.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>CUIT微信校务管理后台</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap.min.css" />
<link rel="stylesheet" href="<%=path%>/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<%=path%>/css/unicorn.main.css" />
<link rel="stylesheet" href="<%=path%>/css/buttons.css" />
<link rel="stylesheet" href="<%=path%>/css/unicorn.grey.css"
	class="skin-color" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<div id="header">
		<h2>CUIT微信</h2>
		<h3>校务管理平台</h3>
	</div>

	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>信息与公告</a>
		<ul>
			<li><a href="<%=path%>/Page/index.jsp"><i
					class="icon icon-home"></i> <span>首页</span></a></li>
			<li class="active submenu open"><a href="#"><i
					class="icon icon-th-list"></i> <span>教务通知</span> <span
					class="label">2</span></a>
				<ul>
					<li><a href="test_list">考试安排</a></li>
					<li><a href="inf_list">信息与公告</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-th"></i>
					<span>选修和教室查询</span> <span class="label">2</span></a>
				<ul>
					<li><a href="elective_list">公共选修</a></li>
					<li><a href="classroom_list">自习室安排</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-pencil"></i>
					<span>图书管理</span><span class="label">2</span></a>
				<ul>
					<li><a href="lib_add">添加图书</a></li>
					<li><a href="lib_querylist">图书列表</a></li>
				</ul></li>
			<li class="submenu"><a href="#"><i class="icon icon-bell"></i>
					<span>学籍信息</span> <span class="label">2</span></a>
				<ul>
					<li><a href="<%=path%>/Page/student_add.jsp">添加学生</a></li>
					<li><a href="student_querylistAction">查看学生</a></li>
				</ul></li>
		</ul>

	</div>


	<div id="content">
		<div id="content-header">
			<h1>信息与公告</h1>
		</div>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i
				class="icon-home"></i> Home</a> <a href="#" class="current">信息与公告</a>
		</div>

		<a href="inf_add" class="btn btn-primary"
			style="margin-left: 20px; margin-top: 20px">添加信息</a>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<div class="widget-box">
						<div class="widget-title">
							<span class="icon"> <i class="icon-align-list"></i>
							</span>
							<h5>通知列表</h5>
						</div>
						<div class="widget-content">
							<s:iterator id="list" value="inflist">
								<div class="widget-content nopadding">
									<div class="alert alert-success alert-block">
										<a class="close" onclick="del(this)"
											id="<s:property value="infoId" />">×</a>
										<h2 class="alert-heading" onclick="edit(this)"
											id="<s:property value="infoId" />">
											<s:property value="infocontent" />
										</h2>
										<s:property value="info_time" />
									</div>
								</div>
							</s:iterator>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<%=path%>/js/jquery.min.js"></script>
	<script src="<%=path%>/js/jquery.ui.custom.js"></script>
	<script src="<%=path%>/js/bootstrap.min.js"></script>
	<script src="<%=path%>/js/unicorn.js"></script>
	<script src="<%=path%>/js/sweetalert.min.js"></script>
	<script type="text/javascript">
		function del(obj) {
			var id = $(obj).attr("id");
			swal({
				title : "确定删除?",
				text : "删除之后不可恢复!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, delete it!",
				closeOnConfirm : false
			}, function() {
				swal("Deleted!", "删除成功！！", "success");
				window.location.href = "inf_deleteByid?id=" + id;
			});
		}
		function edit(object) {
			var id = $(object).attr("id");
			window.location.href = "inf_edit?id=" + id;
		}
	</script>
</body>

</html>
