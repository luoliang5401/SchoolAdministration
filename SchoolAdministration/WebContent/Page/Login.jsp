<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html lang="zh">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>欢迎使用CUIT微信校务管理平台</title>
<link rel="stylesheet" type="text/css" href="<%=path%>/css/styles.css">
<link rel="stylesheet" type="text/css"
	href="<%=path%>/css/sweetalert.css">

</head>

<body>

	<div class="wrapper">
		<div class="container">
			<h1>Welcome</h1>
			<form class="form" id="subUserForm">
				<input type="text" placeholder="用户名" id="login" name="adm">
				<input type="password" placeholder="密码" name="pwd" id="pwd">
				<button type="submit" id="login-button">登录</button>
			</form>
		</div>

		<ul class="bg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>

	<script type="text/javascript" src="<%=path%>/js/jquery.min.js"></script>
	<script src="<%=path%>/js/sweetalert.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#login-button').click(function(event) {
				var params = $("#subUserForm").serialize();
				event.preventDefault();
				$('form').fadeOut(500);
				$('.wrapper').addClass('form-success');
				$.ajax({
					url : 'LoginAction_islogin',
					type : 'post',
					data : params,
					dataType : 'json',//设置需要返回的数据类型
					success : function(data) {
						if (data.message == '') {
							setTimeout(function() {
								window.location.href = "LoginAction_use"
							}, 2000);
						} else {
							sweetAlert(data.message);
							setTimeout(function() {
								window.location.href = "LoginAction_login"
							}, 2000);
						}
					},
				});
			});
		});
	</script>

</body>

</html>